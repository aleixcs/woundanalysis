//
//  MyManager.m
//  Scanner
//
//  Created by Seidor Labs on 11/01/17.
//  Copyright © 2017 Occipital. All rights reserved.
//

#define HAS_LIBCXX
#import <Foundation/Foundation.h>
#import <Structure/Structure.h>
#import "MyManager.h"
#import "CustomerListVC.h"


@implementation MyManager

@synthesize patientID;
@synthesize scanID;
@synthesize categorizationString;
@synthesize colorKeyFrames;
@synthesize depthKeyFrames;
@synthesize categories;
@synthesize meshTexture;





#pragma mark Singleton Methods

+ (id)sharedManager {
    static MyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
        // create keyFrames arrays to store the keyframes
        colorKeyFrames = [[NSMutableArray alloc] init];
        depthKeyFrames = [[NSMutableArray alloc] init];
        
        
        // create categorization dictionary
        NSMutableDictionary *categoriesDict = [NSMutableDictionary dictionary];
        
        // keys for each category
        NSArray *categoriesKeys = [NSArray arrayWithObjects:@"prof_pellIntacta",@"prof_dermisEpidermis", @"prof_subcutani", @"prof_muscul", @"prof_os", @"vores_delimitades", @"vores_regular", @"vores_irregular", @"vores_difoses",@"vores_engrossiment", @"tipusLesio_UPP", @"tipusLesio_UPPHumitat", @"tipusLesio_quirurgica", @"tipusLesio_tumoral", @"tipusLesio_venosa", @"tipusLesio_arterial", @"tipusTeixit_tancada", @"tipusTeixit_epitelitzacio", @"tipusTeixit_granulacio", @"tipusTeixit_hipergranulacio", @"tipusTeixit_granulacioEsfacelat", @"tipusTeixit_esfacelat", @"tipusTeixit_necroticEsfacelat", @"tipusTeixit_necrotic", @"exsudat_sec", @"exsudat_humit", @"exsudat_saturat", @"exsudat_purulent", @"exsudat_seros", @"perilesional_edema", @"perilesional_eritema", nil];
        // strings for each category
        NSArray *categoriesValues = [NSArray arrayWithObjects:@"Pell intacta cicatritzada", @"Afectació de la dermis-epidermis",@"Afectació de teixit subcutani (teixit adipós sense arribar a la fàscia del múscul)", @"Afectació múscul", @"Afectació del os i teixits annexes (tendons, lligaments)", @"Delimitades", @"Regular", @"Irregulars", @"Difoses", @"Engrossiment", @"UPP", @"UPP per humitat", @"Ferida quirúrgica", @"Lesions tumorals", @"Lesió venosa", @"Lesió arterial", @"Tancada/Cicatritzada", @"Teixit de epitelització", @"Teixit de granulació", @"Hipergranulació", @"Teixit de granulació amb presencia teixit esfacelat", @"Teixit esfacelat", @"Teixit necròtic amb presència de teixit esfacelat", @"Teixit necròtic", @"Sec", @"Humit", @"Saturat", @"Purulent", @"Seròs", @"Edema perilesional", @"Eritema perilesional", nil];

        // assign keys and values to the dictionary
        for(int i=0; i <categoriesKeys.count;i++){
            NSString *value = [categoriesValues objectAtIndex:i];
            NSString *key = [categoriesKeys objectAtIndex:i];
            [categoriesDict setValue:value forKey:key];
        }
        self.categories = categoriesDict;
        

            
    }
    return self;
}


- (void)setScanID{
    // Set scanID from the current date and time
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:ss"];
    self.scanID = [dateFormatter stringFromDate:[NSDate date]];
}

- (NSString*)getScanID{
    
    return scanID;
}


-(void)clearKeyFrames{
    // Reset keyframes container
    colorKeyFrames = [[NSMutableArray alloc] init];
    depthKeyFrames = [[NSMutableArray alloc] init];
    
}


- (void)setPatientID:(NSString*) pID {
    patientID = pID;
}

- (NSString*)getPatientID {
    // return always the patientID of the used patient (this line will be removed)
    patientID = @"e14d73b7-8266-4f40-87c5-3cc5047218b4";
    return patientID;
}


- (void)setCategorizationString:(NSString*) cat {
    categorizationString = cat;
}

- (NSString*)getCategorizationString {
    return categorizationString;
}



- (void)setMeshTexture:(UIImage *)tex{
    // Store the mesh texture as a property because in order to be able to use it at the PaintVC class
    meshTexture = [[UIImage alloc] init];
    meshTexture = tex;
}

- (UIImage*)getMeshTexture {
    return meshTexture;
}


- (void)addColorKeyFrame:(STColorFrame*) cf {
    [colorKeyFrames addObject:cf];
}

- (void)addDepthKeyFrame:(STDepthFrame*) df {
    [depthKeyFrames addObject:df];
}




-(UIImage*)CVImageBufferRefToUIImage:(CVImageBufferRef)imageBuffer
{
    // Function to convert a CVImageBufferRef to a UIImage
    UIImage* theImage;
    
    // Lock the base address of the pixel buffer.
    CVPixelBufferLockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);
    
    // Get the data size for contiguous planes of the pixel buffer.
    size_t bufferSize = CVPixelBufferGetDataSize(imageBuffer);
    OSType pixelFormat = CVPixelBufferGetPixelFormatType(imageBuffer);
    
    // Get the pixel buffer width and height.
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    CGImageRef newImage = NULL;
    
    
    if (CVPixelBufferIsPlanar(imageBuffer))
    {
        if (pixelFormat == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) { //420f
            
            // Bi-Planar Component Y'CbCr 8-bit 4:2:0, video-range (luma=[16,235] chroma=[16,240]).
            //http://en.wikipedia.org/wiki/Chroma_subsampling
            // 4:2:0 (J:a:b)
            // J - horizontal sampling reference (width of the conceptual region). Usually, 4.
            // a - number of chrominance samples (Cr, Cb) in the first row of J pixels.
            // b - number of (additional) chrominance samples (Cr, Cb) in the second row of J pixels.
            // CbCr values are packed (in reverse order - so CrCb) in the second plane of the buffer (which makes it double-width, as per BytesPerRow).
            //  - also note that this is half the vertical resolution of the Y plane because these values are stored for every second row.
            
            // For example with a video resolution of 480x360
            // Y Plane - 480x360
            // CbCr Plane - 240x180 (but double-width, so BytesPerRow is 480)
            // Buffer Size: 260648 bytes
            //  480x360 = 172800 (Y Plane)
            //  480x180 = 86400 (CbCr Plane)
            //  1448 (Descriptor struct size)
            
            uint8_t *planeBaseAddressY = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
            size_t planeWidthY = CVPixelBufferGetWidthOfPlane(imageBuffer, 0);
            size_t planeHeightY = CVPixelBufferGetHeightOfPlane(imageBuffer, 0);
            size_t planeBytesPerRowY = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 0);
            
            uint8_t *planeBaseAddressCbCr = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 1);
            size_t planeWidthCbCr = CVPixelBufferGetWidthOfPlane(imageBuffer, 1);
            size_t planeHeightCbCr = CVPixelBufferGetHeightOfPlane(imageBuffer, 1);
            size_t planeBytesPerRowCbCr = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer, 1);
            
            size_t rgba_bytes_per_row = width * 4;
            size_t rgba_buffer_size = height * rgba_bytes_per_row;
            uint8_t* rgba_base_address = (uint8_t*)malloc(rgba_buffer_size);
            
            //Cb,Cr pairs used in a block of 4 pixels in the output image (no interpolation)
            uint8_t pxY, pxCb, pxCr, pxR, pxB, pxG;
            size_t row0, row1, rowCbCr, col0, col1,row0_out,row1_out;
            size_t base_index;
            //half-height 0..180
            
            for (int row = 0; row < planeHeightCbCr; row++) {
                
                row0 = 2 * row * planeBytesPerRowY;
                row0_out = 2 * row * planeWidthY;
                
                row1 = row0 + planeBytesPerRowY;
                row1_out = row0_out + planeWidthY;
                
                rowCbCr = row * planeBytesPerRowCbCr;
                for (int col = 0; col < planeWidthCbCr; col++) {
                    
                    
                    col0 = 2 * col;
                    col1 = col0 + 1;
                    
                    pxCr = planeBaseAddressCbCr[rowCbCr + col0];
                    pxCb = planeBaseAddressCbCr[rowCbCr + col1];
                    
                    
                    //rgba px 0,0
                    pxY = planeBaseAddressY[row0 + col0];
                    [self convertYCbCrtoRGBwithColourfromPxY:pxY fromPxCb:pxCb fromPxCr:pxCr toPxR:&pxR toPxG:&pxG toPxB:&pxB];
                    base_index = (row0_out + col0) * 4;
                    rgba_base_address[base_index]   = pxR;
                    rgba_base_address[base_index+1] = pxG;
                    rgba_base_address[base_index+2] = pxB;
                    rgba_base_address[base_index+3] = 1;
                    
                    //rgba px 0,1
                    pxY = planeBaseAddressY[row0 + col1];
                    [self convertYCbCrtoRGBwithColourfromPxY:pxY fromPxCb:pxCb fromPxCr:pxCr toPxR:&pxR toPxG:&pxG toPxB:&pxB];
                    base_index = (row0_out + col1) * 4;
                    rgba_base_address[base_index]   = pxR;
                    rgba_base_address[base_index+1] = pxG;
                    rgba_base_address[base_index+2] = pxB;
                    rgba_base_address[base_index+3] = 1;
                    
                    //rgba px 1,0
                    pxY = planeBaseAddressY[row1 + col0];
                    [self convertYCbCrtoRGBwithColourfromPxY:pxY fromPxCb:pxCb fromPxCr:pxCr toPxR:&pxR toPxG:&pxG toPxB:&pxB];
                    base_index = (row1_out + col0) * 4;
                    rgba_base_address[base_index]   = pxR;
                    rgba_base_address[base_index+1] = pxG;
                    rgba_base_address[base_index+2] = pxB;
                    rgba_base_address[base_index+3] = 1;
                    
                    //rgba px 1,1
                    pxY = planeBaseAddressY[row1 + col1];
                    [self convertYCbCrtoRGBwithColourfromPxY:pxY fromPxCb:pxCb fromPxCr:pxCr toPxR:&pxR toPxG:&pxG toPxB:&pxB];
                    base_index = (row1_out + col1) * 4;
                    rgba_base_address[base_index]   = pxR;
                    rgba_base_address[base_index+1] = pxG;
                    rgba_base_address[base_index+2] = pxB;
                    rgba_base_address[base_index+3] = 1;
                    
                }
            }
            
            
            
            
            static CGColorSpaceRef colorSpace = NULL;
            if (colorSpace == NULL) // Handle the error appropriately.
                colorSpace = CGColorSpaceCreateDeviceRGB();
            
            CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgba_base_address, rgba_buffer_size,myProviderReleaseData);
            
            
            newImage = CGImageCreate(width, height, 8, 32, rgba_bytes_per_row, colorSpace,
                                     kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Little,
                                     dataProvider, NULL, true, kCGRenderingIntentDefault);
            CGDataProviderRelease(dataProvider);
            
        }
    }
    
    
    
    if (newImage != NULL) {
        theImage = [UIImage imageWithCGImage: newImage ];
        CGImageRelease( newImage );
    }
    
    // Unlock buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer, kCVPixelBufferLock_ReadOnly);


    return theImage;

}


-(void)sendUIImageToServer:(UIImage*)img withKFNumber:(int)n{
    // Function to send UIImages to the server. It will be used to send the key frames
    
    NSData* imgData;
    bool encodingJPG;
    encodingJPG = false;
    
    NSString *keyFrameNumber = [NSString stringWithFormat:@"%d",n];
    
    if(encodingJPG){
        imgData = UIImageJPEGRepresentation(img, 0.6);
    }
    else{
        imgData = UIImagePNGRepresentation(img);
    }

    // send keyframes to the server
    patientID = @"e14d73b7-8266-4f40-87c5-3cc5047218b4";

    
    NSString* urlString = @"http://seidorhealth.westeurope.cloudapp.azure.com/Scanner3D/getKeyFrameFromScanner.php";
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    NSString *boundary = @"STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    
    NSMutableData *body = [NSMutableData data];
    //we can assume boundary as a seperator for data. Your this first statement is starting point for sending multipart data
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // PATIENT ID
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"patientID"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",patientID] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // SCAN ID
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"scanID"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",scanID] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // KEYFRAME NUMBER
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"keyFrameNumber"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",keyFrameNumber] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // KEYFRAME
    if(encodingJPG){
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"keyframe.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];    }
    else{
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"keyframe.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"keyframe.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *myData = imgData;
    [body appendData:myData];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSError *returnError = nil;
    NSHTTPURLResponse *returnResponse = nil;
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&returnResponse error:&returnError];
}


- (void)convertYCbCrtoRGBwithColourfromPxY:(uint8_t)pxY fromPxCb:(uint8_t)pxCb fromPxCr:(uint8_t)pxCr toPxR:(uint8_t*)pxR toPxG:(uint8_t*)pxG toPxB:(uint8_t*)pxB
{
    // Function to convert a pixel in YCbCr format to RGB. It is used by CVImageBufferRefToUIImage.
    int nPxY, nPxCb, nPxCr;
    nPxY = (int)pxY;
    nPxCb = (int)pxCb;
    nPxCr = (int)pxCr;
    
    int nR, nG, nB;
    nR = nPxY                                   +      1.402 * (nPxCr-128);
    nG = nPxY   -   0.344136 * (nPxCb - 128)    -   0.714136 * (nPxCr-128);
    nB = nPxY   +     1.772  * (nPxCb - 128);
    
    if (nR < 0) nR = 0;
    else if (nR > 255) nR = 255;
    
    if (nG < 0) nG = 0;
    else if (nG > 255) nG = 255;
    
    if (nB < 0) nB = 0;
    else if (nB > 255) nB = 255;
    
    *pxR = (uint8_t)nR;
    *pxG = (uint8_t)nG;
    *pxB = (uint8_t)nB;
    
    
    /*
     //    //For standard definition TV applications (SDTV) the following equation describes the color conversion from RGB to YCbCr (according to ITU-R BT.601):
     //    //http://software.intel.com/sites/products/documentation/hpc/ipp/ippi/ippi_ch6/functn_YCbCrToRGB.html
     //    //R' = 1.164*(Y' - 16) + 1.596*(Cr' - 128)
     //    //G' = 1.164*(Y' - 16) - 0.392*(Cb' - 128) - 0.813*(Cr' - 128)
     //    //B' = 1.164*(Y' - 16) + 2.017*(Cb' - 128)
     //
     //    //An alternative possibility could be that they always use ITU-R BT.709 (typically only for HD Video)
     //    //http://www.equasys.de/colorconversion.html
     //    //R' = 1.164*(Y' - 16) + 1.793*(Cr' - 128)
     //    //G' = 1.164*(Y' - 16) - 0.213*(Cb' - 128) - 0.533*(Cr' - 128)
     //    //B' = 1.164*(Y' - 16) + 2.112*(Cb' - 128)
     //
     //    //NB. trying this without checking pxY < 235. Thinking values greater are pulled into bounds when 16 subtracted?
     //    int nPxY, nPxCb, nPxCr;
     //    if (conversion != kYCbCr_FullRange) {
     //        nPxY = pxY < 16 ? 0 : (int)pxY - 16;
     //        nPxCb = pxCb < 16 ? -112 : (int)pxCb - 128;
     //        nPxCr = pxCr < 16 ? -112 : (int)pxCr - 128;
     //    }
     //    else {
     //        nPxY = (int)pxY;
     //        nPxCb = (int)pxCb;
     //        nPxCr = (int)pxCr;
     //    }
     //
     //    int nR, nG, nB;
     //    switch (conversion) {
     //        case kYCbCr_ITU_BT601:
     //            nR = 1.164 * nPxY + 1.596 * nPxCr;
     //            nG = 1.164 * nPxY - 0.392 * nPxCb - 0.813 * nPxCr;
     //            nB = 1.164 * nPxY + 2.017 * nPxCb;
     //            break;
     //        case kYCbCr_ITU_BT709:
     //            nR = 1.164 * nPxY + 1.793 * nPxCr;
     //            nG = 1.164 * nPxY - 0.213 * nPxCb - 0.533 * nPxCr;
     //            nB = 1.164 * nPxY + 2.112 * nPxCb;
     //            break;
     //        case kYCbCr_FullRange:
     //            nR = nPxY + 1.4 * nPxCr;
     //            nG = nPxY - 0.343 * nPxCb - 0.711 * nPxCr;
     //            nB = nPxY + 1.765 * nPxCb;
     //        default:
     //            break;
     //    }
     //
     //    if (nR < 0) nR = 0;
     //    else if (nR > 255) nR = 255;
     //
     //    if (nG < 0) nG = 0;
     //    else if (nG > 255) nG = 255;
     //
     //    if (nB < 0) nB = 0;
     //    else if (nB > 255) nB = 255;
     //
     //    *pxR = (uint8_t)nR;
     //    *pxG = (uint8_t)nG;
     //    *pxB = (uint8_t)nB;
     */
}


void myProviderReleaseData (void *info,	const void *data, size_t size)
{
    //free frame data (when CGImage is freed)
    free((void*)data);
}




- (void)dealloc {
    // Should never be called, but just here for clarity really.
}



-(void) showAlert:(NSString*)title withText:(NSString*)text {
    // Function to show popup alerts. Mainly for debugging, since actual debugging is not possible with the sensor plugged and STWirelessLog has not ben used.
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:text
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


- (void) getPatients:(id)delegate{
    
    // URL for the request
    NSString *urlAsString = [NSString stringWithFormat:@"http://seidorhealth.westeurope.cloudapp.azure.com:1880/getpatients"];
    
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
    
    // Make the request
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
        }
        else {
            
            NSArray *tableData = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
            
            NSLog(@"Number of items in my array is: %d", [tableData count]);//
            myPatients = tableData;
            
            // Send the patients to the CustomerList class
            [delegate receivePatients:tableData];
            
        }
    }];
    
    
}



@end
