//
//  PatientEntity.h
//  Scanner
//
//  Created by David Reifs on 31/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PatientEntity : NSObject

@property (nonatomic, strong) NSString *patientIdentifier;
@property (nonatomic, strong) NSString *patientGivenName;
@property (nonatomic, strong) NSString *patientFamilyName;




@end
