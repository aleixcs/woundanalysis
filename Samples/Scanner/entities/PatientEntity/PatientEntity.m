//
//  PatientEntity.m
//  Scanner
//
//  Created by David Reifs on 31/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import "PatientEntity.h"

@implementation PatientEntity
@synthesize patientIdentifier,patientGivenName,patientFamilyName;

@end
