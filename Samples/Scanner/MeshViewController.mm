/*
 This file is part of the Structure SDK.
 Copyright © 2016 Occipital, Inc. All rights reserved.
 http://structure.io
 */

#import "MeshViewController.h"
#import "MeshRenderer.h"
#import "ViewpointController.h"
#import "CustomUIKitStyles.h"
#import "MyManager.h"
#import "CategorizationVC.h"
#import "PaintVC.h"

#import <ImageIO/ImageIO.h>

#include <vector>
#include <cmath>

// Local Helper Functions
namespace
{
    
    void saveJpegFromRGBABuffer(const char* filename, unsigned char* src_buffer, int width, int height)
    {
        FILE *file = fopen(filename, "w");
        if(!file)
            return;
        
        CGColorSpaceRef colorSpace;
        CGImageAlphaInfo alphaInfo;
        CGContextRef context;
        
        colorSpace = CGColorSpaceCreateDeviceRGB();
        alphaInfo = kCGImageAlphaNoneSkipLast;
        context = CGBitmapContextCreate(src_buffer, width, height, 8, width * 4, colorSpace, alphaInfo);
        CGImageRef rgbImage = CGBitmapContextCreateImage(context);
        
        CGContextRelease(context);
        CGColorSpaceRelease(colorSpace);
        
        CFMutableDataRef jpgData = CFDataCreateMutable(NULL, 0);
        
        CGImageDestinationRef imageDest = CGImageDestinationCreateWithData(jpgData, CFSTR("public.jpeg"), 1, NULL);
        CFDictionaryRef options = CFDictionaryCreate(kCFAllocatorDefault, // Our empty IOSurface properties dictionary
                                                     NULL,
                                                     NULL,
                                                     0,
                                                     &kCFTypeDictionaryKeyCallBacks,
                                                     &kCFTypeDictionaryValueCallBacks);
        CGImageDestinationAddImage(imageDest, rgbImage, (CFDictionaryRef)options);
        CGImageDestinationFinalize(imageDest);
        CFRelease(imageDest);
        CFRelease(options);
        CGImageRelease(rgbImage);
        
        fwrite(CFDataGetBytePtr(jpgData), 1, CFDataGetLength(jpgData), file);
        fclose(file);
        CFRelease(jpgData);
    }
    
}

@interface MeshViewController ()
{
    STMesh *_mesh;
    CADisplayLink *_displayLink;
    MeshRenderer *_renderer;
    ViewpointController *_viewpointController;
    GLfloat _glViewport[4];
    
    GLKMatrix4 _modelViewMatrixBeforeUserInteractions;
    GLKMatrix4 _projectionMatrixBeforeUserInteractions;
}

@property MFMailComposeViewController *mailViewController;
@property (nonatomic, strong) JotViewController *jotViewController;


@end

@implementation MeshViewController

@synthesize mesh = _mesh;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Enrere"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(dismissView)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // the email button is changed to point to the sendToPlatform method
    UIBarButtonItem *emailButton = [[UIBarButtonItem alloc] initWithTitle:@"Envia al servidor"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(sendToPlatform)];
    self.navigationItem.rightBarButtonItem = emailButton;
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Seidor Labs - Anàlisi de ferides";
    }
    
    return self;
}

- (void)setupGestureRecognizer
{
    UIPinchGestureRecognizer *pinchScaleGesture = [[UIPinchGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(pinchScaleGesture:)];
    [pinchScaleGesture setDelegate:self];
    [self.view addGestureRecognizer:pinchScaleGesture];
    
    // We'll use one finger pan for rotation.
    UIPanGestureRecognizer *oneFingerPanGesture = [[UIPanGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(oneFingerPanGesture:)];
    [oneFingerPanGesture setDelegate:self];
    [oneFingerPanGesture setMaximumNumberOfTouches:1];
    [self.view addGestureRecognizer:oneFingerPanGesture];
    
    // We'll use two fingers pan for in-plane translation.
    UIPanGestureRecognizer *twoFingersPanGesture = [[UIPanGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(twoFingersPanGesture:)];
    [twoFingersPanGesture setDelegate:self];
    [twoFingersPanGesture setMaximumNumberOfTouches:2];
    [twoFingersPanGesture setMinimumNumberOfTouches:2];
    [self.view addGestureRecognizer:twoFingersPanGesture];
}

- (void) viewDidAppear:(BOOL)animated{
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    MyManager *sharedManager = [MyManager sharedManager];
    [self setToColor];

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.meshViewerMessageLabel.alpha = 0.0;
    self.meshViewerMessageLabel.hidden = true;
    
    [self.meshViewerMessageLabel applyCustomStyleWithBackgroundColor:blackLabelColorWithLightAlpha];
    
    _renderer = new MeshRenderer();
    _viewpointController = new ViewpointController(self.view.frame.size.width,
                                                   self.view.frame.size.height);
    
    UIFont *font = [UIFont boldSystemFontOfSize:14.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    
    [self.displayControl setTitleTextAttributes:attributes
                                       forState:UIControlStateNormal];
    
    [self setupGestureRecognizer];
}

- (void)setToColor{
    // to load the view with the "colorize" option selected (not working properly)
    self.displayControl.selectedSegmentIndex = 2;
    [self trySwitchToColorRenderingMode];
    
    bool meshIsColorized = [_mesh hasPerVertexColors] || [_mesh hasPerVertexUVTextureCoords];
    if ( !meshIsColorized ) [self colorizeMesh];
    self.needsDisplay = TRUE;

}

- (void)setLabel:(UILabel*)label enabled:(BOOL)enabled {
    
    UIColor* whiteLightAlpha = [UIColor colorWithRed:1.0  green:1.0   blue:1.0 alpha:0.5];
    
    if(enabled)
        [label setTextColor:[UIColor whiteColor]];
    else
        [label setTextColor:whiteLightAlpha];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_displayLink)
    {
        [_displayLink invalidate];
        _displayLink = nil;
    }
    
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(draw)];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    
    _viewpointController->reset();
    
    if (!self.colorEnabled)
        [self.displayControl removeSegmentAtIndex:2 animated:NO];
    
    self.displayControl.selectedSegmentIndex = 1;
    _renderer->setRenderingMode( MeshRenderer::RenderingModeLightedGray );
    
    
    
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    // show categorization at screen if we have introduced it and we are coming back to the mesh view
    // if the view is appearing after a back button is pressed in the next view in the stack (the view controller is already in the navigation controller's stack, and another view controller is being popped to reveal it.)
    MyManager *sharedManager = [MyManager sharedManager];
    if(!self.isMovingToParentViewController){
        // actually it is also false when the view appears for the first time, but it works anyway for this purpose
        // show categorization with the mesh
        [self.categorizationLabel setText:[sharedManager getCategorizationString]];
        self.categorizationLabel.numberOfLines=0; //set number of lines to 0 in order to visualize the carriage returns
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupGL:(EAGLContext *)context
{
    [(EAGLView*)self.view setContext:context];
    [EAGLContext setCurrentContext:context];
    
    _renderer->initializeGL();
    
    [(EAGLView*)self.view setFramebuffer];
    CGSize framebufferSize = [(EAGLView*)self.view getFramebufferSize];
    
    float imageAspectRatio = 1.0f;
    
    // The iPad's diplay conveniently has a 4:3 aspect ratio just like our video feed.
    // Some iOS devices need to render to only a portion of the screen so that we don't distort
    // our RGB image. Alternatively, you could enlarge the viewport (losing visual information),
    // but fill the whole screen.
    if ( std::abs(framebufferSize.width/framebufferSize.height - 640.0f/480.0f) > 1e-3)
        imageAspectRatio = 480.f/640.0f;
    
    _glViewport[0] = (framebufferSize.width - framebufferSize.width*imageAspectRatio)/2;
    _glViewport[1] = 0;
    _glViewport[2] = framebufferSize.width*imageAspectRatio;
    _glViewport[3] = framebufferSize.height;
}

- (void)dismissView
{
    if ([self.delegate respondsToSelector:@selector(meshViewWillDismiss)])
        [self.delegate meshViewWillDismiss];
    
    // Make sure we clear the data we don't need.
    _renderer->releaseGLBuffers();
    _renderer->releaseGLTextures();
    
    [_displayLink invalidate];
    _displayLink = nil;
    
    self.mesh = nil;
    
    [(EAGLView *)self.view setContext:nil];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if([self.delegate respondsToSelector:@selector(meshViewDidDismiss)])
            [self.delegate meshViewDidDismiss];
    }];
}

#pragma mark - MeshViewer setup when loading the mesh

- (void)setCameraProjectionMatrix:(GLKMatrix4)projection
{
    _viewpointController->setCameraProjection(projection);
    _projectionMatrixBeforeUserInteractions = projection;
}

- (void)resetMeshCenter:(GLKVector3)center
{
    _viewpointController->reset();
    _viewpointController->setMeshCenter(center);
    _modelViewMatrixBeforeUserInteractions = _viewpointController->currentGLModelViewMatrix();
}

- (void)setMesh:(STMesh *)meshRef
{
    _mesh = meshRef;
    
    if (meshRef)
    {
        _renderer->uploadMesh(meshRef);
        
        [self trySwitchToColorRenderingMode];
        
        self.needsDisplay = TRUE;
    }
}

#pragma mark - Email Mesh OBJ file

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self.mailViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareScreenShot:(NSString*)screenshotPath
{
    const int width = 320;
    const int height = 240;
    
    GLint currentFrameBuffer;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &currentFrameBuffer);
    
    // Create temp texture, framebuffer, renderbuffer
    glViewport(0, 0, width, height);
    
    GLuint outputTexture;
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &outputTexture);
    glBindTexture(GL_TEXTURE_2D, outputTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    
    GLuint colorFrameBuffer, depthRenderBuffer;
    glGenFramebuffers(1, &colorFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, colorFrameBuffer);
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, outputTexture, 0);
    
    // Keep the current render mode
    MeshRenderer::RenderingMode previousRenderingMode = _renderer->getRenderingMode();
    
    STMesh* meshToRender = _mesh;
    
    // Screenshot rendering mode, always use colors if possible.
    if ([meshToRender hasPerVertexUVTextureCoords] && [meshToRender meshYCbCrTexture])
    {
        _renderer->setRenderingMode( MeshRenderer::RenderingModeTextured );
    }
    else if ([meshToRender hasPerVertexColors])
    {
        _renderer->setRenderingMode( MeshRenderer::RenderingModePerVertexColor );
    }
    else // meshToRender can be nil if there is no available color mesh.
    {
        _renderer->setRenderingMode( MeshRenderer::RenderingModeLightedGray );
    }
    
    // Render from the initial viewpoint for the screenshot.
    _renderer->clear();
    _renderer->render(_projectionMatrixBeforeUserInteractions, _modelViewMatrixBeforeUserInteractions);
    
    // Back to current render mode
    _renderer->setRenderingMode( previousRenderingMode );
    
    struct RgbaPixel { uint8_t rgba[4]; };
    std::vector<RgbaPixel> screenShotRgbaBuffer (width*height);
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, screenShotRgbaBuffer.data());
    
    // We need to flip the axis, because OpenGL reads out the buffer from the bottom.
    std::vector<RgbaPixel> rowBuffer (width);
    for (int h = 0; h < height/2; ++h)
    {
        RgbaPixel* screenShotDataTopRow    = screenShotRgbaBuffer.data() + h * width;
        RgbaPixel* screenShotDataBottomRow = screenShotRgbaBuffer.data() + (height - h - 1) * width;
        
        // Swap the top and bottom rows, using rowBuffer as a temporary placeholder.
        memcpy(rowBuffer.data(), screenShotDataTopRow, width * sizeof(RgbaPixel));
        memcpy(screenShotDataTopRow, screenShotDataBottomRow, width * sizeof (RgbaPixel));
        memcpy(screenShotDataBottomRow, rowBuffer.data(), width * sizeof (RgbaPixel));
    }
    
    saveJpegFromRGBABuffer([screenshotPath UTF8String], reinterpret_cast<uint8_t*>(screenShotRgbaBuffer.data()), width, height);
    
    // Back to the original frame buffer
    glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBuffer);
    glViewport(_glViewport[0], _glViewport[1], _glViewport[2], _glViewport[3]);
    
    // Free the data
    glDeleteTextures(1, &outputTexture);
    glDeleteFramebuffers(1, &colorFrameBuffer);
    glDeleteRenderbuffers(1, &depthRenderBuffer);
}



// -----------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)saveOnDiskButton:(id)sender
{
    [self saveData];
}



// -----------------------------------------------------------------------------------------------------------------------------------------------
- (void)saveData
{
    // The mesh and the keyframes are stored locally
    
    // Get the paths
    MyManager *sharedManager = [MyManager sharedManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    
    
    // get patientID and scanID
    NSString *scanID = [sharedManager getScanID];
    NSString *patientID = [sharedManager getPatientID];
    
    // create the Scans folder if it does not exist
    NSString *scansPath = [documentsDirectory stringByAppendingPathComponent:@"/Scans"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:scansPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:scansPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }
    
    // create the patientID folder if it does not exist
    NSString *patientIDPath = [scansPath stringByAppendingPathComponent:[@"/" stringByAppendingString:patientID]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:patientIDPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:patientIDPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }

    // create the scanID folder if it does not exist
    NSString *scanIDPath = [patientIDPath stringByAppendingPathComponent:[@"/" stringByAppendingString:scanID]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:scanIDPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:scanIDPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }

    // create the reconstruction folder if it does not exist
    NSString *reconstructionPath = [scanIDPath stringByAppendingPathComponent:@"/reconstruction"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:reconstructionPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:reconstructionPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }

    // create the keyframes folder if it does not exist
    NSString *keyframesPath = [scanIDPath stringByAppendingPathComponent:@"/keyFrames"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:keyframesPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:keyframesPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }
    
    // get the MESH zip file path
    NSString* cacheDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
    NSString* zipFilename = @"Model.zip";
    NSString *zipPath = [cacheDirectory stringByAppendingPathComponent:zipFilename];
    
    // Request a zipped OBJ file, potentially with embedded MTL and texture.
    NSDictionary* options = @{ kSTMeshWriteOptionFileFormatKey: @(STMeshWriteOptionFileFormatObjFileZip) };
    
    STMesh* meshToSend = _mesh;
    BOOL success = [meshToSend writeToFile:zipPath options:options error:&error];
    
    // copy the MESH zip file to the created directory
    NSString* destZipPath = [reconstructionPath stringByAppendingPathComponent:@"/Model.zip"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:destZipPath] == NO) {
        [[NSFileManager defaultManager] copyItemAtPath:zipPath toPath:destZipPath error:&error];
    }
    
    
    // save the keyframes to the created directory
    // for all the keyframes
    for(int i=0;i<sharedManager.colorKeyFrames.count;i++){
        
        // obtain it as STColorFrame and as UIImage and then as NSData
        STColorFrame* cf = [sharedManager.colorKeyFrames objectAtIndex:i];
        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(cf.sampleBuffer);
        UIImage* theImage = [sharedManager CVImageBufferRefToUIImage:imageBuffer];
        NSData* imgData = UIImagePNGRepresentation(theImage);
        
        NSString *keyFrameNumber = [NSString stringWithFormat:@"%d",i];
        
        NSString *thisKeyFramePath = [keyframesPath stringByAppendingString:@"/keyframe_"];
        thisKeyFramePath = [thisKeyFramePath stringByAppendingString:keyFrameNumber];
        thisKeyFramePath = [thisKeyFramePath stringByAppendingString:@".png"];
        
        [imgData writeToFile:thisKeyFramePath atomically:YES]; //Write the file
    }
    
    // save a preview to the scan folder in order to visually identify the scans
    // Setup paths and filenames.
    NSString* screenshotFilename = @"Preview.jpg";
    NSString *screenshotPath =[scanIDPath stringByAppendingPathComponent:screenshotFilename];
    // Take a screenshot and save it to disk.
    [self prepareScreenShot:screenshotPath];
    
}



// -----------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)showTextureButton:(id)sender {

    // Get mesh texture, store it at manager and launch the paint view
    MyManager *sharedManager = [MyManager sharedManager];
    STMesh* meshToSend = _mesh;
    CVImageBufferRef meshTexture = [meshToSend meshYCbCrTexture];
    UIImage* meshTextureUIImage = [sharedManager CVImageBufferRefToUIImage:meshTexture];
    
    [sharedManager setMeshTexture:meshTextureUIImage];
    PaintVC *paintVC = [[PaintVC alloc] init];
    [[self navigationController] pushViewController:paintVC animated:YES];
}


// -----------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)sincronizeButton:(id)sender {
    [self sincronize];
}



// -----------------------------------------------------------------------------------------------------------------------------------------------
- (void)sincronize{
    // Upload all the scans that had been stored to iPad. If it is sent correctly, remove the scans from disk
    
    // Get path
    MyManager *sharedManager = [MyManager sharedManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    
    // list Scans directory
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Scans"];
    
    NSArray *patientsList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    // for all patients
    for (int patient = 0; patient < (int)[patientsList count]; patient++){
        //flag to check for errors
        Boolean uploadedCorrectly = true;
        
        // list patient directory
        NSString* patientID = [patientsList objectAtIndex:patient];
        
        NSString* scansPath = [dataPath stringByAppendingPathComponent:[patientsList objectAtIndex:patient]];
        
        
        NSArray* scansList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:scansPath error:NULL];
        // for all scans
        for (int scan = 0; scan < (int)[scansList count]; scan++){
            
            // UPLOAD MESH
            
            
            
            NSString* scanID = [scansList objectAtIndex:scan];
        
            // Setup paths and filenames.
            NSString *zipPath = [scansPath stringByAppendingPathComponent:[scansList objectAtIndex:scan]];
            zipPath = [zipPath stringByAppendingString:@"/reconstruction/Model.zip"];
            
            // server URL
            NSString *urlStringMesh = @"http://seidorhealth.westeurope.cloudapp.azure.com/Scanner3D/getMeshFromScanner.php";
            NSURL *urlMesh = [NSURL URLWithString:urlStringMesh];
            
            // create request and add boundary and content type
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlMesh];
            [request setHTTPMethod:@"POST"];
            NSString *boundary = @"STRING";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
            [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
            
            // create body and start with a boundary
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // PATIENT ID
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"patientID"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n",patientID] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // SCAN ID
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"scanID"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n",scanID] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // MESH
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"test.zip\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/zip\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            NSData *myData = [NSData dataWithContentsOfFile:zipPath];
            [body appendData:myData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request setHTTPBody:body];
            
            NSError *returnError = nil;
            NSHTTPURLResponse *returnResponse = nil;
            
            // send the request
            NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&returnResponse error:&returnError];
            if(!(returnError.code == 0)){
                uploadedCorrectly = false;
            }
            
            
            
            
            
            
            // UPLOAD KEYFRAMES
            
            
            
            NSString* urlStringKF = @"http://seidorhealth.westeurope.cloudapp.azure.com/Scanner3D/getKeyFrameFromScanner.php";
            NSURL *urlKF = [NSURL URLWithString:urlStringKF];
            
            NSString *keyFramesPath = [scansPath stringByAppendingPathComponent:[[scansList objectAtIndex:scan] stringByAppendingString:@"/keyFrames/"]];
            
            NSArray* keyFramesList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:keyFramesPath error:NULL];
            // for all keyframes
            for (int kf = 0; kf < (int)[keyFramesList count]; kf++){
                
                NSString *keyFrameNumber = [NSString stringWithFormat:@"%d",kf];
                
                NSData* imgData;
                bool encodingJPG;
                encodingJPG = false;
                
                
                NSString* thisKeyFramePath = [keyFramesPath stringByAppendingPathComponent:[keyFramesList objectAtIndex:kf]];
                
                UIImage *img = [UIImage imageWithContentsOfFile:thisKeyFramePath];
                
                if(encodingJPG){
                    imgData = UIImageJPEGRepresentation(img, 0.6);
                }
                else{
                    imgData = UIImagePNGRepresentation(img);
                }
                
                // send keyframes to the server
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlKF];
                [request setHTTPMethod:@"POST"];
                
                
                NSString *boundary = @"STRING";
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
                
                [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
                
                
                NSMutableData *body = [NSMutableData data];
                //we can assume boundary as a seperator for data. Your this first statement is starting point for sending multipart data
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                // PATIENT ID
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"patientID"] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n",patientID] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                // SCAN ID
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"scanID"] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n",scanID] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                // KEYFRAME NUMBER
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"keyFrameNumber"] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n",keyFrameNumber] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                // KEYFRAME
                if(encodingJPG){
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"keyframe.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];    }
                else{
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"keyframe.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"keyframe.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                NSData *myData = imgData;
                [body appendData:myData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [request setHTTPBody:body];
                
                
                
                NSError *returnError = nil;
                NSHTTPURLResponse *returnResponse = nil;
                
                NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&returnResponse error:&returnError];
                if(!(returnError.code == 0)){
                    uploadedCorrectly = false;
                }
            }
        }
        
        // delete the patient directory and its scans if both mesh and keyframes have been correctly added
        if(uploadedCorrectly){
            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:scansPath error:nil];
        }
    }
}



// -----------------------------------------------------------------------------------------------------------------------------------------------
- (void)sendToPlatform
{
    // ---------------------------------------------------------------------------------
    // SEND COLOR KEYFRAMES
    MyManager *sharedManager = [MyManager sharedManager];
    // Get the key frames and sent them using the manager
    for(int i=0;i<sharedManager.colorKeyFrames.count;i++){
        STColorFrame* cf = [sharedManager.colorKeyFrames objectAtIndex:i];
        UIImage* theImage;
        
        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(cf.sampleBuffer);
        theImage = [sharedManager CVImageBufferRefToUIImage:imageBuffer];
        
        [sharedManager sendUIImageToServer:theImage withKFNumber:i];
        
    }
    
    
    // ---------------------------------------------------------------------------------
    // SEND MESH
    // CURRENT IMPLEMENTATION WITH PATIENT ID
    
    NSString *scanID = [sharedManager getScanID];
    NSString *patientID = [sharedManager getPatientID];
    
    
    // Setup paths and filenames.
    
    NSString* cacheDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
    NSString* zipFilename = @"Model.zip";
    NSString *zipPath = [cacheDirectory stringByAppendingPathComponent:zipFilename];
    
    // Request a zipped OBJ file, potentially with embedded MTL and texture.
    NSDictionary* options = @{ kSTMeshWriteOptionFileFormatKey: @(STMeshWriteOptionFileFormatObjFileZip) };
    NSError* error;
    STMesh* meshToSend = _mesh;
    BOOL success = [meshToSend writeToFile:zipPath options:options error:&error];

    
    // server URL
    NSString *urlString = @"http://seidorhealth.westeurope.cloudapp.azure.com/Scanner3D/getMeshFromScanner.php";
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    NSString *boundary = @"STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    
    NSMutableData *body = [NSMutableData data];
    //we can assume boundary as a seperator for data. Your this first statement is starting point for sending multipart data
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // PATIENT ID
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"patientID"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",patientID] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // SCAN ID
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"scanID"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",scanID] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // NOTES
    // [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"notes"] dataUsingEncoding:NSUTF8StringEncoding]];
    // [body appendData:[[NSString stringWithFormat:@"%@\r\n",self.notesText.text] dataUsingEncoding:NSUTF8StringEncoding]];
    // [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // MESH
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; testField=\"testFieldName\"; name=\"fileUpload\"; filename=\"test.zip\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/zip\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *myData = [NSData dataWithContentsOfFile:zipPath];
    [body appendData:myData];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSError *returnError = nil;
    NSHTTPURLResponse *returnResponse = nil;
    // send the request
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&returnResponse error:&returnError];
}



#pragma mark - Rendering

- (void)draw
{
    [(EAGLView *)self.view setFramebuffer];
    
    glViewport(_glViewport[0], _glViewport[1], _glViewport[2], _glViewport[3]);
    
    bool viewpointChanged = _viewpointController->update();
    
    // If nothing changed, do not waste time and resources rendering.
    if (!_needsDisplay && !viewpointChanged)
        return;
    
    GLKMatrix4 currentModelView = _viewpointController->currentGLModelViewMatrix();
    GLKMatrix4 currentProjection = _viewpointController->currentGLProjectionMatrix();
    
    _renderer->clear();
    _renderer->render (currentProjection, currentModelView);
    
    _needsDisplay = FALSE;
    
    [(EAGLView *)self.view presentFramebuffer];
}

#pragma mark - Touch & Gesture control

- (void)pinchScaleGesture:(UIPinchGestureRecognizer *)gestureRecognizer
{
    // Forward to the ViewpointController.
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan)
        _viewpointController->onPinchGestureBegan([gestureRecognizer scale]);
    else if ( [gestureRecognizer state] == UIGestureRecognizerStateChanged)
        _viewpointController->onPinchGestureChanged([gestureRecognizer scale]);
}

- (void)oneFingerPanGesture:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGPoint touchPos = [gestureRecognizer locationInView:self.view];
    CGPoint touchVel = [gestureRecognizer velocityInView:self.view];
    GLKVector2 touchPosVec = GLKVector2Make(touchPos.x, touchPos.y);
    GLKVector2 touchVelVec = GLKVector2Make(touchVel.x, touchVel.y);
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan)
        _viewpointController->onOneFingerPanBegan(touchPosVec);
    else if([gestureRecognizer state] == UIGestureRecognizerStateChanged)
        _viewpointController->onOneFingerPanChanged(touchPosVec);
    else if([gestureRecognizer state] == UIGestureRecognizerStateEnded)
        _viewpointController->onOneFingerPanEnded (touchVelVec);
}

- (void)twoFingersPanGesture:(UIPanGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer numberOfTouches] != 2)
        return;
    
    CGPoint touchPos = [gestureRecognizer locationInView:self.view];
    CGPoint touchVel = [gestureRecognizer velocityInView:self.view];
    GLKVector2 touchPosVec = GLKVector2Make(touchPos.x, touchPos.y);
    GLKVector2 touchVelVec = GLKVector2Make(touchVel.x, touchVel.y);
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan)
        _viewpointController->onTwoFingersPanBegan(touchPosVec);
    else if([gestureRecognizer state] == UIGestureRecognizerStateChanged)
        _viewpointController->onTwoFingersPanChanged(touchPosVec);
    else if([gestureRecognizer state] == UIGestureRecognizerStateEnded)
        _viewpointController->onTwoFingersPanEnded (touchVelVec);
}

- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    _viewpointController->onTouchBegan();
}

#pragma mark - UI Control

- (void)trySwitchToColorRenderingMode
{
    // Choose the best available color render mode, falling back to LightedGray
    
    // This method may be called when colorize operations complete, and will
    // switch the render mode to color, as long as the user has not changed
    // the selector.
    
    MyManager *sharedManager = [MyManager sharedManager];
    
    if(self.displayControl.selectedSegmentIndex == 2)
    {
        if ( [_mesh hasPerVertexUVTextureCoords]){
            _renderer->setRenderingMode(MeshRenderer::RenderingModeTextured);
        }
        else if ([_mesh hasPerVertexColors]){
            _renderer->setRenderingMode(MeshRenderer::RenderingModePerVertexColor);
        }
        else{
            _renderer->setRenderingMode(MeshRenderer::RenderingModeLightedGray);
        }
    }
}


- (IBAction)displayControlChanged:(id)sender {
    
    switch (self.displayControl.selectedSegmentIndex) {
        case 0: // x-ray
        {
            _renderer->setRenderingMode(MeshRenderer::RenderingModeXRay);
        }
            break;
        case 1: // lighted-gray
        {
            _renderer->setRenderingMode(MeshRenderer::RenderingModeLightedGray);
        }
            break;
        case 2: // color
        {
            [self trySwitchToColorRenderingMode];
            
            bool meshIsColorized = [_mesh hasPerVertexColors] ||
            [_mesh hasPerVertexUVTextureCoords];
            
            if ( !meshIsColorized ) [self colorizeMesh];
        }
            break;
        default:
            break;
    }
    
    self.needsDisplay = TRUE;
}

- (void)colorizeMesh
{
    MyManager *sharedManager = [MyManager sharedManager];
    [self.delegate meshViewDidRequestColorizing:_mesh previewCompletionHandler:^{

    } enhancedCompletionHandler:^{
        // Hide progress bar.
        [self hideMeshViewerMessage];
    }];
}

- (void)hideMeshViewerMessage
{
    [UIView animateWithDuration:0.5f animations:^{
        self.meshViewerMessageLabel.alpha = 0.0f;
    } completion:^(BOOL finished){
        [self.meshViewerMessageLabel setHidden:YES];
    }];
}

- (void)showMeshViewerMessage:(NSString *)msg
{
    [self.meshViewerMessageLabel setText:msg];
    
    if (self.meshViewerMessageLabel.hidden == YES)
    {
        [self.meshViewerMessageLabel setHidden:NO];
        
        self.meshViewerMessageLabel.alpha = 0.0f;
        [UIView animateWithDuration:0.5f animations:^{
            self.meshViewerMessageLabel.alpha = 1.0f;
        }];
    }
}



- (IBAction)continue:(id)sender {
    
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    CategorizationVC *categVC = [[CategorizationVC alloc] init];
    [[self navigationController] pushViewController:categVC animated:YES];
    
}






@end
