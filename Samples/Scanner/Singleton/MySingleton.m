//
//  MySingleton.m
//  Scanner
//
//  Created by Seidor Labs on 11/01/17.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import "MySingleton.h"
#import "CustomerListVC.h"

@implementation MySingleton
@synthesize myPatients;

+(MySingleton *)instance
{
    static MySingleton *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
    
}


- (void) getPatients:(id)delegate{
    
    // URL for the request
    NSString *urlAsString = [NSString stringWithFormat:@"http://seidorhealth.westeurope.cloudapp.azure.com:1880/getpatients"];
    
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
    
    // Make the request
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
        }
        else {

            NSArray *tableData = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
            
            NSLog(@"Number of items in my array is: %d", [tableData count]);//
            myPatients = tableData;
            
            // Send the patients to the CustomerList class
            [delegate receivePatients:tableData];

        }
    }];


}



@end
