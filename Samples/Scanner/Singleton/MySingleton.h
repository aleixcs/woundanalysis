//
//  MySingleton.h
//  Scanner
//
//  Created by David Reifs on 31/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PatientEntity.h"

@interface MySingleton : NSObject{

    PatientEntity *selectedPatient;
    NSMutableArray *myPatients;
    
    
}


@property (nonatomic, retain) PatientEntity *selectedPatient;
@property (nonatomic, retain) NSMutableArray *myPatients;


+(id)instance;

- (void) getPatients:(id)delegate;



@end
