/*
 This file is part of the Structure SDK.
 Copyright © 2016 Occipital, Inc. All rights reserved.
 http://structure.io
 */

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Structure/StructureSLAM.h>
#import "EAGLView.h"
#import <jot/jot.h>

// -----------------------------------------------------------------------------------------------------------------------------------------------
#import "classes/CategorizationVC/CategorizationVC.h"

@protocol MeshViewDelegate <NSObject>
- (void)meshViewWillDismiss;
- (void)meshViewDidDismiss;
- (BOOL)meshViewDidRequestColorizing:(STMesh*)mesh
            previewCompletionHandler:(void(^)(void))previewCompletionHandler
           enhancedCompletionHandler:(void(^)(void))enhancedCompletionHandler;
@end

@interface MeshViewController : UIViewController <UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, assign) id<MeshViewDelegate> delegate;

@property (nonatomic) BOOL needsDisplay; // force the view to redraw.
@property (nonatomic) BOOL colorEnabled;
@property (nonatomic) STMesh * mesh;

@property (weak, nonatomic) IBOutlet UISegmentedControl *displayControl;
@property (weak, nonatomic) IBOutlet UILabel *meshViewerMessageLabel;

// -----------------------------------------------------------------------------------------------------------------------------------------------
@property (weak, nonatomic) IBOutlet UIImageView *textureImageView;
@property (nonatomic, retain) IBOutlet UITextView *console;
@property (weak, nonatomic) IBOutlet UITextView *messageLog;
- (IBAction)continue:(id)sender;
- (IBAction)saveOnDiskButton:(id)sender;
- (IBAction)sincronizeButton:(id)sender;
- (IBAction)showTextureButton:(id)sender;
- (IBAction)showCategorizationButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *categorizationLabel;
- (IBAction)switchSegment:(id)sender;


- (IBAction)displayControlChanged:(id)sender;

- (void)showMeshViewerMessage:(NSString *)msg;
- (void)hideMeshViewerMessage;

- (void)setCameraProjectionMatrix:(GLKMatrix4)projRt;
- (void)resetMeshCenter:(GLKVector3)center;

- (void)setupGL:(EAGLContext*)context;

@end
