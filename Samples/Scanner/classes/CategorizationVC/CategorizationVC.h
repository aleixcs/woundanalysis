//
//  CategorizationVC.h
//  Scanner
//
//  Created by Seidor Labs on 03/04/17.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategorizationVC : UIViewController

- (IBAction)saveButton:(id)sender;

// Profunditat
@property (weak, nonatomic) IBOutlet UISwitch *prof_pellIntacta;
@property (weak, nonatomic) IBOutlet UISwitch *prof_dermisEpidermis;
@property (weak, nonatomic) IBOutlet UISwitch *prof_subcutani;
@property (weak, nonatomic) IBOutlet UISwitch *prof_muscul;
@property (weak, nonatomic) IBOutlet UISwitch *prof_os;

// Vores
@property (weak, nonatomic) IBOutlet UISwitch *vores_delimitades;
@property (weak, nonatomic) IBOutlet UISwitch *vores_regular;
@property (weak, nonatomic) IBOutlet UISwitch *vores_irregular;
@property (weak, nonatomic) IBOutlet UISwitch *vores_difoses;
@property (weak, nonatomic) IBOutlet UISwitch *vores_engrossiment;


// Tipus de lesió
@property (weak, nonatomic) IBOutlet UISwitch *tipusLesio_UPP;
@property (weak, nonatomic) IBOutlet UISwitch *tipusLesio_UPPHumitat;
@property (weak, nonatomic) IBOutlet UISwitch *tipusLesio_quirurgica;
@property (weak, nonatomic) IBOutlet UISwitch *tipusLesio_tumoral;
@property (weak, nonatomic) IBOutlet UISwitch *tipusLesio_venosa;
@property (weak, nonatomic) IBOutlet UISwitch *tipusLesio_arterial;

// Tipus de teixit
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_tancada;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_epitelitzacio;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_granulacio;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_hipergranulacio;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_granulacioEsfacelat;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_esfacelat;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_necroticEsfacelat;
@property (weak, nonatomic) IBOutlet UISwitch *tipusTeixit_necrotic;

// Exsudat
@property (weak, nonatomic) IBOutlet UISwitch *exsudat_sec;
@property (weak, nonatomic) IBOutlet UISwitch *exsudat_humit;
@property (weak, nonatomic) IBOutlet UISwitch *exsudat_saturat;
@property (weak, nonatomic) IBOutlet UISwitch *exsudat_purulent;
@property (weak, nonatomic) IBOutlet UISwitch *exsudat_seros;

// Zona perilesional
@property (weak, nonatomic) IBOutlet UISwitch *perilesional_edema;
@property (weak, nonatomic) IBOutlet UISwitch *perilesional_eritema;



@end
