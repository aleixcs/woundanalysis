//
//  CategorizationVC.m
//  Scanner
//
//  Created by Seidor Labs on 03/04/17.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import "CategorizationVC.h"
#import "DBManager.h"
#import "MyManager.h"
#import "MeshViewController.h"


@interface CategorizationVC ()
@property (nonatomic, strong) DBManager *dbManager;


@end

@implementation CategorizationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveButton:(id)sender {
    
    
    MyManager *sharedManager = [MyManager sharedManager];
    // get patientID and scanID
    NSString *scanID = [sharedManager getScanID];
    NSString *patientID = [sharedManager getPatientID];
    NSLog(@"%@", patientID);


    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb3.sql"];

    
    // Prepare the query string.
    NSString *query8 = [NSString stringWithFormat:@"insert into scans values(null, '%@', '%@', '%@', %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)", scanID, patientID, @"sampleWoundID", [self switchIsOn:self.prof_pellIntacta],[self switchIsOn:self.prof_dermisEpidermis],[self switchIsOn:self.prof_subcutani], [self switchIsOn:self.prof_muscul], [self switchIsOn:self.prof_os], [self switchIsOn:self.vores_delimitades], [self switchIsOn:self.vores_regular], [self switchIsOn:self.vores_irregular], [self switchIsOn:self.vores_difoses], [self switchIsOn:self.vores_engrossiment],[self switchIsOn:self.tipusLesio_UPP], [self switchIsOn:self.tipusLesio_UPPHumitat], [self switchIsOn:self.tipusLesio_quirurgica], [self switchIsOn:self.tipusLesio_tumoral], [self switchIsOn:self.tipusLesio_venosa], [self switchIsOn:self.tipusLesio_arterial],[self switchIsOn:self.tipusTeixit_tancada], [self switchIsOn:self.tipusTeixit_epitelitzacio], [self switchIsOn:self.tipusTeixit_granulacio], [self switchIsOn:self.tipusTeixit_hipergranulacio], [self switchIsOn:self.tipusTeixit_granulacioEsfacelat], [self switchIsOn:self.tipusTeixit_esfacelat], [self switchIsOn:self.tipusTeixit_necroticEsfacelat], [self switchIsOn:self.tipusTeixit_necrotic],[self switchIsOn:self.exsudat_sec], [self switchIsOn:self.exsudat_humit], [self switchIsOn:self.exsudat_saturat], [self switchIsOn:self.exsudat_purulent], [self switchIsOn:self.exsudat_seros],[self switchIsOn:self.perilesional_edema], [self switchIsOn:self.perilesional_eritema] ];
    
    
    int a = [self switchIsOn:self.prof_pellIntacta];
    
    NSLog(@"%@", query8);
    
    // Execute the query.
    [self.dbManager executeQuery:query8];
    
    // If the query was successfully executed
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    
    
    // show the table contents
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb3.sql"];
    NSArray *arrPeopleInfo;
    
    NSString *query2 = @"select * from scans";
    
    // Get the results.
    if (arrPeopleInfo != nil) {
        arrPeopleInfo = nil;
    }
    arrPeopleInfo = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query2]];
    
    NSString *tableLog = @"";
    for(int i=0; i<arrPeopleInfo.count;i++){
        NSArray *row = [arrPeopleInfo objectAtIndex:i];
        NSString *rowString = @"";
        for(int j=0; j<row.count;j++){
            rowString = [rowString stringByAppendingString:[row objectAtIndex:j]];
            rowString = [rowString stringByAppendingString:@" - "];
        }
        tableLog = [tableLog stringByAppendingString:rowString];
        NSLog(@"%@", rowString);
        if(i == arrPeopleInfo.count-1){
        }
    }

    
    
    
    
    
    
    // create the string to show the categorization with the mesh
    
    // show the table contents
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb3.sql"];
    NSArray *resultsArray;
    
    
    NSString *query = [@"select * from scans where scanTimeID = '"  stringByAppendingString:[[sharedManager getScanID] stringByAppendingString:@"'"]];
    
    // Get the results.
    if (resultsArray != nil) {
        resultsArray = nil;
    }
    resultsArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSMutableArray *columns = self.dbManager.arrColumnNames;
    
    
    NSString *labelText = @"";
    Boolean atLeastOne1 = false;
    for(int i=0; i<resultsArray.count;i++){
        if(i == resultsArray.count-1){
            NSArray *row = [resultsArray objectAtIndex:i];
            for(int j=0; j<row.count;j++){
                if(j>3){
                    if(j==4){
                        labelText = [labelText stringByAppendingString:@"\n\n- Profunditat:\n"];
                    }
                    if(j==9){
                        labelText = [labelText stringByAppendingString:@"\n\n- Vores:\n"];
                    }
                    if(j==13){
                        labelText = [labelText stringByAppendingString:@"\n\n- Tipus de lesió:\n"];
                    }
                    if(j==19){
                        labelText = [labelText stringByAppendingString:@"\n\n- Tipus de teixit:\n"];
                    }
                    if(j==27){
                        labelText = [labelText stringByAppendingString:@"\n\n- Exsudat:\n"];
                    }
                    if(j==32){
                        labelText = [labelText stringByAppendingString:@"\n\n- Zona perilesional:\n"];
                    }
                    
                    if([[row objectAtIndex:j]  isEqual: @"1"]){
                        atLeastOne1 = true;
                        labelText = [labelText stringByAppendingString:[sharedManager.categories objectForKey:[columns objectAtIndex:j]]];
                        labelText = [labelText stringByAppendingString:@", "];
                    }
                }
            }
            if(atLeastOne1){
                [sharedManager setCategorizationString:labelText];
            }
            else{
                [sharedManager setCategorizationString:@""];
            }
        }
    }

    
    
    
    
    
    
    // go back to the MeshViewController
    [self.navigationController popViewControllerAnimated:YES];
    

}

- (int) switchIsOn:(UISwitch*)theSwitch{
    Boolean prof_pellIntactaState = [theSwitch isOn];
    return prof_pellIntactaState ? 1 : 0;
}
@end
