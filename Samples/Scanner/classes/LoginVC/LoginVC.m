//
//  LoginVC.m
//  Scanner
//
//  Created by David Reifs on 30/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import "LoginVC.h"
#import "CustomerListVC.h"
#import "CategorizationVC.h"


@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doCustomerList:(id)sender {
    
    CustomerListVC *customerListVC = [[CustomerListVC alloc] init];
    [[self navigationController] pushViewController:customerListVC animated:YES];    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
