//
//  CustomerDetailVC.m
//  Scanner
//
//  Created by David Reifs on 30/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import "CustomerDetailVC.h"
#import "ViewController.h"
#import "MyManager.h"



@interface CustomerDetailVC ()
@property (weak, nonatomic) IBOutlet UILabel *UIPatientID;
@property (weak, nonatomic) IBOutlet UILabel *UIPatientFamilyName;


@end

@implementation CustomerDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    // Get and set to screen the patient information
    [_UIPatientID setText:[[[MyManager sharedManager] selectedPatient] valueForKey:@"identifier"]];
    [_UIPatientFamilyName setText:[[[MyManager sharedManager] selectedPatient] valueForKey:@"family_name"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)doScan:(id)sender {
    
    // Launch the ViewController to start scanning
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
    [[self navigationController] pushViewController:self.viewController animated:YES];
}

@end
