//
//  PaintVC.h
//  
//
//  Created by Seidor Labs on 15/05/17.
//
//

#import <UIKit/UIKit.h>
#import <jot/jot.h>
#import "MyManager.h"


@interface PaintVC : UIViewController <JotViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *textureIV;
@property (nonatomic, strong) JotViewController *jotViewController;
@property (nonatomic, strong) UIImage *meshTexture;



@end
