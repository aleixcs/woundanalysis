//
//  PaintVC.m
//  
//
//  Created by Seidor Labs on 15/05/17.
//
//

#import "PaintVC.h"


@interface PaintVC ()

@end

@implementation PaintVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    MyManager *sharedManager = [MyManager sharedManager];
    self.meshTexture = [sharedManager getMeshTexture];
    [self.textureIV setImage:self.meshTexture];
    if([self.textureIV isHidden]){
        [self.textureIV setHidden:NO];
    }
    
    

    
    // -------------------------------------------------------------------------------------------------------------------------------------------------
    // jot view controller to draw on images
    
    _jotViewController = [JotViewController new];
    self.jotViewController.delegate = self;
    
    [self addChildViewController:self.jotViewController];
    [self.view addSubview:self.jotViewController.view];
    [self.jotViewController didMoveToParentViewController:self];
    self.jotViewController.view.frame = self.view.frame;
    
    self.jotViewController.state = JotViewStateDrawing;
    
    return [self.jotViewController drawOnImage:self.meshTexture];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
