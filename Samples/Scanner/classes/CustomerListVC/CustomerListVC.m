//
//  CustomerListVC.m
//  Scanner
//
//  Created by David Reifs on 30/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import "CustomerListVC.h"
#import "CustomerDetailVC.h"
#import "PatientEntity.h"
#import "MyManager.h"



@interface CustomerListVC () <UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, strong) IBOutlet UITableView *tableViewPatients;
@property (nonatomic, strong) NSMutableArray *arrayPatients;

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) MyManager *sharedManager;



@end

@implementation CustomerListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.array = [[NSMutableArray alloc] init];
    
    _sharedManager = [MyManager sharedManager];
    [_sharedManager getPatients:self];
    
    [self.tableViewPatients reloadData];
    [self.tableViewPatients setDelegate:self];
    [self.tableViewPatients setDataSource:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)doCustomerDetail:(id)sender {
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrayPatients count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    PatientEntity *patient = self.arrayPatients[indexPath.row];
    
    [cell.textLabel setText:[patient valueForKey:@"identifier"]];
    [cell.detailTextLabel setText:[patient valueForKey:@"family_name"]];
    
    NSLog(@"%@",[patient valueForKey:@"identifier"]);
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"selected row");
    
    PatientEntity *mypatient = self.arrayPatients[indexPath.row];
    
    [_sharedManager setSelectedPatient:mypatient];

    CustomerDetailVC *detailVC = [[CustomerDetailVC alloc] init];
    [[self navigationController] pushViewController:detailVC animated:YES];
}




- (void) receivePatients:(NSMutableArray*)array
{    
    self.arrayPatients = array;
    [self.tableViewPatients reloadData];
    
    NSLog(@"Number of items in my array is: %lu", (unsigned long)[self.arrayPatients count]);//
    NSLog(@"receiveProjects");
}


@end
