//
//  CustomerListVC.h
//  Scanner
//
//  Created by David Reifs on 30/03/2017.
//  Copyright © 2017 Occipital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerListVC : UIViewController

- (void) receivePatients:(NSMutableArray*)array;

@end
