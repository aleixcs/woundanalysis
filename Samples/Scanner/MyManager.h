//
//  MyManager.h
//  Scanner
//
//  Created by Seidor Labs on 11/01/17.
//  Copyright © 2017 Occipital. All rights reserved.
//
#define HAS_LIBCXX
#import <Structure/Structure.h>
#import "entities/PatientEntity/PatientEntity.h"



@interface MyManager : NSObject {
    PatientEntity *selectedPatient;
    NSMutableArray *myPatients;
}

@property (nonatomic, retain) NSString *patientID;
@property (nonatomic, retain) NSString *scanID;
@property (nonatomic, retain) NSString *categorizationString;
@property (nonatomic, retain) NSMutableArray *colorKeyFrames;
@property (nonatomic, retain) NSMutableArray *depthKeyFrames;
@property (nonatomic, retain) NSDictionary *categories;
@property (nonatomic, strong) UIImage *meshTexture;
@property (nonatomic, retain) PatientEntity *selectedPatient;
@property (nonatomic, retain) NSMutableArray *myPatients;


+ (id)sharedManager;
- (void)setPatientID:(NSString*) pID;
- (void)setScanID;
- (void)setMeshTexture:(UIImage *)tex;
- (UIImage*)getMeshTexture;
- (NSString*)getScanID;
- (NSString*)getCategorizationString;
- (NSString*)getPatientID;
- (void)addColorKeyFrame:(STColorFrame*) cf;
- (void)addDepthKeyFrame:(STDepthFrame*) df;
- (void)convertYCbCrtoRGBwithColourfromPxY:(uint8_t)pxY fromPxCb:(uint8_t)pxCb fromPxCr:(uint8_t)pxCr toPxR:(uint8_t*)pxR toPxG:(uint8_t*)pxG toPxB:(uint8_t*)pxB;
- (void)sendUIImageToServer:(UIImage*)img withKFNumber:(int)n;
- (UIImage*)CVImageBufferRefToUIImage:(CVImageBufferRef)imageBuffer;
- (void)showAlert:(NSString*)title withText:(NSString*)text;
- (void)clearKeyFrames;
- (void) getPatients:(id)delegate;












@end
