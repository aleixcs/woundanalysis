/*
  This file is part of the Structure SDK.
  Copyright © 2015 Occipital, Inc. All rights reserved.
  http://structure.io
*/

#import <GLKit/GLKit.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h> // GL_RED_EXT


#import "MeshRenderer.h"
#import "CustomShaders.h"

#import <Structure/StructureSLAM.h>

#define MAX_MESHES 30

extern void glDrawBuffers (GLsizei n, const GLenum *bufs);


// Local functions

struct MeshRenderer::PrivateData
{
    LightedGrayShader lightedGrayShader;
    PerVertexColorShader perVertexColorShader;
    XrayShader xRayShader;
    YCbCrTextureShader yCbCrTextureShader;
    
    int numUploadedMeshes = 0;
    int numTriangleIndices[MAX_MESHES];
    int numLinesIndices[MAX_MESHES];

    bool hasPerVertexColor = false;
    bool hasPerVertexNormals = false;
    bool hasPerVertexUV = false;
    bool hasTexture = false;
    
    // Vertex buffer objects.
    GLuint vertexVbo[MAX_MESHES];
    GLuint normalsVbo[MAX_MESHES];
    GLuint colorsVbo[MAX_MESHES];
    GLuint texcoordsVbo[MAX_MESHES];
    GLuint facesVbo[MAX_MESHES];
    GLuint linesVbo[MAX_MESHES];

    // OpenGL Texture reference for y and chroma images (and mask).
    CVOpenGLESTextureRef lumaTexture = NULL;
    CVOpenGLESTextureRef chromaTexture = NULL;
    CVOpenGLESTextureRef maskTexture = NULL; //-----------------------------------------------------------------------------------------------

    // OpenGL Texture cache for the color texture.
    CVOpenGLESTextureCacheRef textureCache = NULL;
    
    // Texture unit to use for texture binding/rendering.
    GLenum textureUnit = GL_TEXTURE3;
    
    // Current render mode.
    RenderingMode currentRenderingMode = RenderingModeLightedGray;
    
    GLuint frameBuffer;
    GLuint UVTexture;
    GLuint colorTexture;
    
};



MeshRenderer::MeshRenderer()
: d (new PrivateData)
{
}

void MeshRenderer::initializeGL (GLenum defaultTextureUnit)
{
    d->textureUnit = defaultTextureUnit;
    glGenBuffers (MAX_MESHES, d->vertexVbo);
    glGenBuffers (MAX_MESHES, d->normalsVbo);
    glGenBuffers (MAX_MESHES, d->colorsVbo);
    glGenBuffers (MAX_MESHES, d->texcoordsVbo);
    glGenBuffers (MAX_MESHES, d->facesVbo);
    glGenBuffers (MAX_MESHES, d->linesVbo);    
}

void MeshRenderer::releaseGLTextures ()
{
    if (d->lumaTexture)
    {
        CFRelease (d->lumaTexture);
        d->lumaTexture = NULL;
    }
    
    if (d->chromaTexture)
    {
        CFRelease(d->chromaTexture);
        d->chromaTexture = NULL;
    }
    
    if (d->maskTexture) //-----------------------------------------------------------------------------------------------
    {
        CFRelease(d->maskTexture);
        d->maskTexture = NULL;
    }
    
    
    if (d->textureCache)
    {
        CFRelease(d->textureCache);
        d->textureCache = NULL;
    }
}

void MeshRenderer::releaseGLBuffers ()
{
    for (int meshIndex = 0; meshIndex < d->numUploadedMeshes; ++meshIndex)
    {
        glBindBuffer(GL_ARRAY_BUFFER, d->vertexVbo[meshIndex]);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ARRAY_BUFFER, d->normalsVbo[meshIndex]);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ARRAY_BUFFER, d->colorsVbo[meshIndex]);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ARRAY_BUFFER, d->texcoordsVbo[meshIndex]);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d->facesVbo[meshIndex]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d->linesVbo[meshIndex]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
    }
}

MeshRenderer::~MeshRenderer()
{
    if(d->vertexVbo[0])
        glDeleteBuffers(MAX_MESHES, d->vertexVbo);
    
    if(d->normalsVbo[0])
        glDeleteBuffers(MAX_MESHES, d->normalsVbo);
    
    if(d->colorsVbo[0])
        glDeleteBuffers(MAX_MESHES, d->colorsVbo);
    
    if(d->texcoordsVbo[0])
        glDeleteBuffers(MAX_MESHES, d->texcoordsVbo);
    
    if(d->facesVbo[0])
        glDeleteBuffers(MAX_MESHES, d->facesVbo);
    
    if(d->linesVbo[0])
        glDeleteBuffers(MAX_MESHES, d->linesVbo);
    
    releaseGLTextures ();

    delete d;
}

void MeshRenderer::clear()
{
    if(d->currentRenderingMode == RenderingModePerVertexColor || d->currentRenderingMode == RenderingModeTextured)
        glClearColor(0.9, 0.9, 0.9, 1.0);
    else
        glClearColor(0.1, 0.1, 0.1, 1.0);
    
    glClearDepthf(1.0);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MeshRenderer::setRenderingMode( RenderingMode mode )
{
    d->currentRenderingMode = mode;
}

MeshRenderer::RenderingMode MeshRenderer::getRenderingMode() const
{
    return d->currentRenderingMode;
}

void MeshRenderer::uploadMesh (STMesh* mesh)
{
    int numUploads = fmin((int)[mesh numberOfMeshes], MAX_MESHES);
    d->numUploadedMeshes = fmin((int)[mesh numberOfMeshes], MAX_MESHES);
    
    d->hasPerVertexColor = [mesh hasPerVertexColors];
    d->hasPerVertexNormals = [mesh hasPerVertexNormals];
    d->hasPerVertexUV = [mesh hasPerVertexUVTextureCoords];
    d->hasTexture = ([mesh meshYCbCrTexture] != NULL);

    if (d->hasTexture)
        
         

                  
    
    
        uploadTexture ([mesh meshYCbCrTexture]);
    

    
    
    for (int meshIndex = 0; meshIndex < numUploads; ++meshIndex)
    {
        const int numVertices = [mesh numberOfMeshVertices:meshIndex];
        
        glBindBuffer(GL_ARRAY_BUFFER, d->vertexVbo[meshIndex]);
        glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof (GLKVector3), [mesh meshVertices:meshIndex], GL_STATIC_DRAW);
        
        if (d->hasPerVertexNormals)
        {
            glBindBuffer(GL_ARRAY_BUFFER, d->normalsVbo[meshIndex]);
            glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof (GLKVector3), [mesh meshPerVertexNormals:meshIndex], GL_STATIC_DRAW);
        }
        
        if (d->hasPerVertexColor)
        {
            glBindBuffer(GL_ARRAY_BUFFER, d->colorsVbo[meshIndex]);
            glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof (GLKVector3), [mesh meshPerVertexColors:meshIndex], GL_STATIC_DRAW);
        }
        
        if (d->hasPerVertexUV)
        {
            glBindBuffer(GL_ARRAY_BUFFER, d->texcoordsVbo[meshIndex]);
            glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof (GLKVector2), [mesh meshPerVertexUVTextureCoords:meshIndex], GL_STATIC_DRAW);
        }
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d->facesVbo[meshIndex]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, [mesh numberOfMeshFaces:meshIndex] * sizeof(unsigned short) * 3,
                     [mesh meshFaces:meshIndex], GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d->linesVbo[meshIndex]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, [mesh numberOfMeshLines:meshIndex] * sizeof(unsigned short) * 2,
                     [mesh meshLines:meshIndex], GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        d->numTriangleIndices[meshIndex] = [mesh numberOfMeshFaces:meshIndex] * 3;
        d->numLinesIndices[meshIndex] = [mesh numberOfMeshLines:meshIndex] * 2;
    }
}




























void MeshRenderer::uploadTexture (CVImageBufferRef pixelBuffer)
{
    
    
    
    // OLD VERSION OF THE FUNCTION. CURRENT VERSION IS AT THE END BUT HAS TO BE CHECKED.
    
    int width = (int)CVPixelBufferGetWidth(pixelBuffer);
    int height = (int)CVPixelBufferGetHeight(pixelBuffer);
    
    EAGLContext *context = [EAGLContext currentContext];
    assert (context != nil);
    
    releaseGLTextures ();
    
    if (d->textureCache == NULL)
    {
        CVReturn texError = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, context, NULL, &d->textureCache);
        if (texError) { NSLog(@"Error at CVOpenGLESTextureCacheCreate %d", texError); }
    }
    
    // Allow the texture cache to do internal cleanup.
    CVOpenGLESTextureCacheFlush(d->textureCache, 0);
    
    OSType pixelFormat = CVPixelBufferGetPixelFormatType (pixelBuffer);
    assert(pixelFormat == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //      Y
    // Activate the default texture unit.
    glActiveTexture (d->textureUnit);
    
    // Create a new Y texture from the video texture cache.
    CVReturn err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                d->textureCache,
                                                                pixelBuffer,
                                                                NULL,
                                                                GL_TEXTURE_2D,
                                                                GL_RED_EXT,
                                                                (int)width,
                                                                (int)height,
                                                                GL_RED_EXT,
                                                                GL_UNSIGNED_BYTE,
                                                                0,
                                                                &d->lumaTexture);
    
    if (err){
        NSLog(@"Error with CVOpenGLESTextureCacheCreateTextureFromImage: %d", err);
        return;
    }
    // Set rendering properties for the new texture.
    glBindTexture(CVOpenGLESTextureGetTarget(d->lumaTexture), CVOpenGLESTextureGetName(d->lumaTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //      CbCr
    // Activate the next texture unit for CbCr.
    glActiveTexture (d->textureUnit + 1);
    // Create a new CbCr texture from the video texture cache.
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       d->textureCache,
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_RG_EXT,
                                                       (int)width/2,
                                                       (int)height/2,
                                                       GL_RG_EXT,
                                                       GL_UNSIGNED_BYTE,
                                                       1,
                                                       &d->chromaTexture);
    if (err){
        NSLog(@"Error with CVOpenGLESTextureCacheCreateTextureFromImage: %d", err);
        return;
    }
    glBindTexture(CVOpenGLESTextureGetTarget(d->chromaTexture), CVOpenGLESTextureGetName(d->chromaTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // APPROACH 1:  Create a pixel buffer by copying the current one
    
    /*
     
     
     // Get pixel buffer info
     CVPixelBufferLockBaseAddress(pixelBuffer, 0);
     int bufferWidth = (int)CVPixelBufferGetWidth(pixelBuffer);
     int bufferHeight = (int)CVPixelBufferGetHeight(pixelBuffer);
     size_t bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer);
     uint8_t *baseAddress = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
     
     // Copy the pixel buffer
     CVPixelBufferRef pixel_buffer4e = NULL;
     // format stored in pixelFormat
     //CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, bufferWidth, bufferHeight, kCVPixelFormatType_32BGRA, NULL, &pixel_buffer4e);
     CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, bufferWidth, bufferHeight, pixelFormat, NULL, &pixel_buffer4e);
     CVPixelBufferLockBaseAddress(pixel_buffer4e, 0);
     uint8_t *copyBaseAddress = (uint8_t*)CVPixelBufferGetBaseAddress(pixel_buffer4e);
     memcpy(copyBaseAddress, baseAddress, bufferHeight * bytesPerRow);
     
     
     */
    
    
    
    // APPROACH 2: Create a pixel buffer from scratch
    
    CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault, // EMPTY IOSURFACE DICT
                                               NULL,
                                               NULL,
                                               0,
                                               &kCFTypeDictionaryKeyCallBacks,
                                               &kCFTypeDictionaryValueCallBacks);
    CFMutableDictionaryRef attributes = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                                  1,
                                                                  &kCFTypeDictionaryKeyCallBacks,
                                                                  &kCFTypeDictionaryValueCallBacks);
    
    CFDictionarySetValue(attributes,kCVPixelBufferIOSurfacePropertiesKey,empty);
    CVPixelBufferRef pixel_buffer4e = NULL;
    
    
    CVReturn status2 = CVPixelBufferCreate(kCFAllocatorDefault,
                                           (int)CVPixelBufferGetWidth(pixelBuffer),
                                           (int)CVPixelBufferGetHeight(pixelBuffer),
                                           kCVPixelFormatType_420YpCbCr8BiPlanarFullRange,
                                           attributes,
                                           &pixel_buffer4e);
    
    // inicialitzar buffer
    const int kBytesPerPixel = 1;
    CVPixelBufferLockBaseAddress( pixel_buffer4e, 0 );
    int bufferWidth = (int)CVPixelBufferGetWidth( pixelBuffer );
    int bufferHeight = (int)CVPixelBufferGetHeight( pixelBuffer );
    uint8_t *pixel = (uint8_t *)CVPixelBufferGetBaseAddress( pixel_buffer4e );
    
    for ( int row = 0; row < bufferHeight/2; row++ )
    {
        for ( int column = 0; column < bufferWidth/2; column++ )
        {
            pixel[0] = ((column/4)%2==0?255:0); // BGRA, Blue value
            //pixel[1] = 127; // Green value
            //pixel[2] = 127; // Red value
            pixel += kBytesPerPixel;
        }
    }
    
    CVPixelBufferUnlockBaseAddress( pixel_buffer4e, 0 );
    
    
    /*
     
     // :::: ALERT :::::
     NSMutableString* aString1 = [NSMutableString stringWithFormat:@"Width: %d ", (int)CVPixelBufferGetWidth(pixelBuffer)];    // does not need to be released. Needs to be retained if you need to keep use it after the current function.
     [aString1 appendFormat:@"Height: %d ", (int)CVPixelBufferGetHeight(pixelBuffer)];
     
     if(true){
     UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"Alert pixelBuffer"
     message:aString1
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert3 show];
     }
     // :::: /ALERT :::::
     
     //
     
     */
    
    //     MASK //-----------------------------------------------------------------------------------------------
    // Activate the next texture unit for MASK.
    glActiveTexture (d->textureUnit + 2);
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       d->textureCache,
                                                       pixel_buffer4e,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_RED_EXT,
                                                       (int)width,
                                                       (int)height,
                                                       GL_RED_EXT,
                                                       GL_UNSIGNED_BYTE,
                                                       0,
                                                       &d->maskTexture);
    
    if (err){
        NSLog(@"Error with CVOpenGLESTextureCacheCreateTextureFromImage: %d", err);
        return;
    }
    
    // Set rendering properties for the new texture.
    glBindTexture(CVOpenGLESTextureGetTarget(d->maskTexture), CVOpenGLESTextureGetName(d->maskTexture));
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    
    
    
    
    /*
     
     // :::: ALERT :::::
     NSMutableString* aString = [NSMutableString stringWithFormat:@"Width: %d ", (int)CVPixelBufferGetWidth(pixel_buffer4e)];    // does not need to be released. Needs to be retained if you need to keep use it after the current function.
     [aString appendFormat:@"Height: %d ", (int)CVPixelBufferGetHeight(pixel_buffer4e)];
     
     if(true){
     UIAlertView *alert3 = [[UIAlertView alloc] initWithTitle:@"Alert pixel_buffer4e"
     message:aString1
     delegate:nil
     cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert3 show];
     }
     // :::: /ALERT :::::
     
     
     */
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // CURRENT NOT WORKING VERSION
    
//    
//
//    int width = (int)CVPixelBufferGetWidth(pixelBuffer);
//    int height = (int)CVPixelBufferGetHeight(pixelBuffer);
//    
//    EAGLContext *context = [EAGLContext currentContext];
//    assert (context != nil);
//    
//    releaseGLTextures ();
//    
//    if (d->textureCache == NULL)
//    {
//        CVReturn texError = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, context, NULL, &d->textureCache);
//        if (texError) { NSLog(@"Error at CVOpenGLESTextureCacheCreate %d", texError); }
//    }
//    
//    // Allow the texture cache to do internal cleanup.
//    CVOpenGLESTextureCacheFlush(d->textureCache, 0);
//    
//    OSType pixelFormat = CVPixelBufferGetPixelFormatType (pixelBuffer);
//    assert(pixelFormat == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange);
//    
//
//    
//    
//    
//    
//    
//    
//
//
//    
//    
//    
//    //      Y
//    // Activate the default texture unit.
//    glActiveTexture (d->textureUnit);
//    
//    // Create a new Y texture from the video texture cache.
//    CVReturn err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
//                                                                d->textureCache,
//                                                                pixelBuffer,
//                                                                NULL,
//                                                                GL_TEXTURE_2D,
//                                                                GL_RED_EXT,
//                                                                (int)width,
//                                                                (int)height,
//                                                                GL_RED_EXT,
//                                                                GL_UNSIGNED_BYTE,
//                                                                0,
//                                                                &d->lumaTexture);
//    
//    if (err){
//        NSLog(@"Error with CVOpenGLESTextureCacheCreateTextureFromImage: %d", err);
//        return;
//    }
//    // Set rendering properties for the new texture.
//    glBindTexture(CVOpenGLESTextureGetTarget(d->lumaTexture), CVOpenGLESTextureGetName(d->lumaTexture));
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    
//    
//    
//    
//    
//    
//    
//    //      CbCr
//    // Activate the next texture unit for CbCr.
//    glActiveTexture (d->textureUnit + 1);
//    // Create a new CbCr texture from the video texture cache.
//    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
//                                                       d->textureCache,
//                                                       pixelBuffer,
//                                                       NULL,
//                                                       GL_TEXTURE_2D,
//                                                       GL_RG_EXT,
//                                                       (int)width/2,
//                                                       (int)height/2,
//                                                       GL_RG_EXT,
//                                                       GL_UNSIGNED_BYTE,
//                                                       1,
//                                                       &d->chromaTexture);
//    if (err){
//        NSLog(@"Error with CVOpenGLESTextureCacheCreateTextureFromImage: %d", err);
//        return;
//    }
//    glBindTexture(CVOpenGLESTextureGetTarget(d->chromaTexture), CVOpenGLESTextureGetName(d->chromaTexture));
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    
//
//    
//    
//    
//    
//    
//    
//    
//    
//    ////////////////////////////
//    ////////////////////////////
//    ////////////////////////////
//    // TASK 1: create an OpenGL Texture to save the wound mask
//    ////////////////////////////
//    ////////////////////////////
//    ////////////////////////////
//    
//    
//    // APPROACH 2: Create a pixel buffer from scratch
//    
//    CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault, // EMPTY IOSURFACE DICT
//                                               NULL,
//                                               NULL,
//                                               0,
//                                               &kCFTypeDictionaryKeyCallBacks,
//                                               &kCFTypeDictionaryValueCallBacks);
//    CFMutableDictionaryRef attributes = CFDictionaryCreateMutable(kCFAllocatorDefault,
//                                                                  1,
//                                                                  &kCFTypeDictionaryKeyCallBacks,
//                                                                  &kCFTypeDictionaryValueCallBacks);
//    
//    CFDictionarySetValue(attributes,kCVPixelBufferIOSurfacePropertiesKey,empty);
//    CVPixelBufferRef pixel_buffer4e = NULL;
//    
//    
//    CVReturn status2 = CVPixelBufferCreate(kCFAllocatorDefault,
//                                           (int)CVPixelBufferGetWidth(pixelBuffer),
//                                           (int)CVPixelBufferGetHeight(pixelBuffer),
//                                           kCVPixelFormatType_420YpCbCr8BiPlanarFullRange,
//                                           attributes,
//                                           &pixel_buffer4e);
//    
//    // inicialitzar buffer
//    const int kBytesPerPixel = 1;
//    CVPixelBufferLockBaseAddress( pixel_buffer4e, 0 );
//    int bufferWidth = (int)CVPixelBufferGetWidth( pixelBuffer );
//    int bufferHeight = (int)CVPixelBufferGetHeight( pixelBuffer );
//    uint8_t *pixel = (uint8_t *)CVPixelBufferGetBaseAddress( pixel_buffer4e );
//    
//    for ( int row = 0; row < bufferHeight/2; row++ )
//    {
//        for ( int column = 0; column < bufferWidth/2; column++ )
//        {
//            pixel[0] = ((column/4)%2==0?255:0); // BGRA, Blue value
//            //pixel[1] = 127; // Green value
//            //pixel[2] = 127; // Red value
//            pixel += kBytesPerPixel;
//        }
//    }
//                
//    CVPixelBufferUnlockBaseAddress( pixel_buffer4e, 0 );
//    
//    
//    
//    //     MASK //-----------------------------------------------------------------------------------------------
//    // Activate the next texture unit for MASK.
//    glActiveTexture (d->textureUnit + 2);
//    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
//                                                                d->textureCache,
//                                                                pixel_buffer4e,
//                                                                NULL,
//                                                                GL_TEXTURE_2D,
//                                                                GL_RED_EXT,
//                                                                (int)width,
//                                                                (int)height,
//                                                                GL_RED_EXT,
//                                                                GL_UNSIGNED_BYTE,
//                                                                0,
//                                                                &d->maskTexture);
//    
//    if (err){
//        NSLog(@"Error with CVOpenGLESTextureCacheCreateTextureFromImage: %d", err);
//        return;
//    }
//    
//    // Set rendering properties for the new texture.
//    glBindTexture(CVOpenGLESTextureGetTarget(d->maskTexture), CVOpenGLESTextureGetName(d->maskTexture));
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//    
//    
//
//    
//    
//
//    
//    
//    glBindTexture(GL_TEXTURE_2D, 0);
//    
//    
//    
//    
//    
//    
//    
//    
//    ////////////////////////////
//    ////////////////////////////
//    ////////////////////////////
//    // TASK 1 ended
//    ////////////////////////////
//    ////////////////////////////
//    ////////////////////////////
//    
//    
//    
//    ////////////////////////////
//    ////////////////////////////
//    ////////////////////////////
//    // TASK 2: create a custom renderBuffer with two layers and attach it to the default frameBuffer
//    ////////////////////////////
//    ////////////////////////////
//    ////////////////////////////
//    
////
////    
////    
////    
////    
////    
////    // EXAMPLE:  CREATE A FRAME BUFFER AND ATTACH A COLOR RENDER BUFFER TO IT
////    
////    bool showExample = true;
////    
////    if(showExample){
////    
////        // Create the framebuffer and bind it.
////        GLuint framebuffer;
////        glGenFramebuffers(1, &framebuffer);
////        glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
////        // :::: ALERT :::::
////        showAlert(@"FrameBuffer example alert",[NSMutableString stringWithFormat:@"framebuffer created"]);
////
////
////        
////        
////        
////        // Create a color renderbuffer, allocate storage for it, and attach it to the framebuffer’s color attachment point.
////        GLuint colorRenderbuffer;
////        glGenRenderbuffers(1, &colorRenderbuffer);
////        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
////        int renderBufferWidth = 1920;
////        int renderBufferHeight = 1280;
////        glRenderbufferStorage(GL_RENDERBUFFER, GL_SRGB8_ALPHA8_EXT, renderBufferWidth, renderBufferHeight);
////        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
////        // :::: ALERT :::::
////        showAlert(@"FrameBuffer example alert",[NSMutableString stringWithFormat:@"render buffer (colorrenderbuffer) created, storage allocated and attached"]);
////        
////        
////        //Create a depth or depth/stencil renderbuffer, allocate storage for it, and attach it to the framebuffer’s depth attachment point.
////        GLuint depthRenderbuffer;
////        glGenRenderbuffers(1, &depthRenderbuffer);
////        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
////        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, renderBufferWidth, renderBufferHeight);
////        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
////        // :::: ALERT :::::
////        showAlert(@"FrameBuffer example alert",[NSMutableString stringWithFormat:@"depth render buffer (depthrenderbuffer) created, storage allocated and attached"]);
////        
////        
////        //Test the framebuffer for completeness. This test only needs to be performed when the framebuffer’s configuration changes.
////        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) ;
////        if(status != GL_FRAMEBUFFER_COMPLETE) {
////            NSLog(@"failed to make complete framebuffer object %x", status);
////            
////            // :::: ALERT :::::
////            showAlert(@"FrameBuffer example alert",[NSMutableString stringWithFormat:@"failed to make complete framebuffer object %x", status]);
////            
////            
////        }
////    }
////    
////    
////    
////    
////    
////    
////    
////    
////    
////    
////    
////    // CREATE A RENDER BUFFER WITH THE SAME SIZE OF THE DEFAULT FRAMEBUFFER
////    
////    /*
////    // bind the framebuffer as the output framebuffer
////    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
////    
////    // define the index array for the outputs
////    GLuint attachments[1] = {GL_COLOR_ATTACHMENT0};
////    glDrawBuffers(1,  attachments);
////    */
////    
////    // get the dimensions of the framebuffer
////    GLint dims[4] = {0};
////    glGetIntegerv(GL_VIEWPORT, dims);
////    GLint fbWidth = dims[2];
////    GLint fbHeight = dims[3];
////    
////    
////    // :::: ALERT :::::
////    showAlert(@"FrameBuffer alert",[NSMutableString stringWithFormat:@"FB Width: %d Height: %d", (int)fbWidth,(int)fbHeight]);
////    
////    
////    
////    
////    // Create the framebuffer and bind it.
////    GLuint framebuffer;
////    glGenFramebuffers(1, &framebuffer);
////    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
////    // :::: ALERT :::::
////    showAlert(@"FrameBuffer example alert",[NSMutableString stringWithFormat:@"framebuffer created"]);
////    
////    
////    
////    
////    
////    // Create a TEXTURE color renderbuffer, allocate storage for it, and attach it to the framebuffer’s FIRST color attachment point.
////    GLuint colorTexture;
////    glGenTextures(1, &colorTexture);
////    glBindTexture(GL_TEXTURE_2D, colorTexture);
////    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, fbWidth, fbHeight,0,GL_RGB,GL_UNSIGNED_BYTE,0);
////    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
////    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
////    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,colorTexture,0);
////    // :::: ALERT :::::
////    showAlert(@"FrameBuffer alert",[NSMutableString stringWithFormat:@"render buffer (colorrenderbuffer) created, storage allocated and attached"]);
////    
////
////    // Create a UV color renderbuffer, allocate storage for it, and attach it to the framebuffer’s FIRST color attachment point.
////    GLuint UVTexture;
////    glGenTextures(1, &UVTexture);
////    glBindTexture(GL_TEXTURE_2D, UVTexture);
////    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, fbWidth, fbHeight,0,GL_RGB,GL_UNSIGNED_BYTE,0);
////    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
////    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
////    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+1, GL_TEXTURE_2D,UVTexture,0);
////    // :::: ALERT :::::
////    showAlert(@"FrameBuffer alert",[NSMutableString stringWithFormat:@"render buffer (maskrenderbuffer) created, storage allocated and attached"]);
////    
////    
////    //Create a DEPTH OR DEPTH/STENCIL renderbuffer, allocate storage for it, and attach it to the framebuffer’s depth attachment point.
////    GLuint depthRenderbuffer;
////    glGenRenderbuffers(1, &depthRenderbuffer);
////    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
////    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, fbWidth, fbHeight);
////    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
////    // :::: ALERT :::::
////    showAlert(@"FrameBuffer alert",[NSMutableString stringWithFormat:@"depth render buffer (depthrenderbuffer) created, storage allocated and attached"]);
////    
////    
////    GLenum DrawBuffers[2]= {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT0+1};
////    //glDrawBuffers(2,DrawBuffers);
////    
////    
////    
////    //Test the framebuffer for completeness. This test only needs to be performed when the framebuffer’s configuration changes.
////    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) ;
////    if(status != GL_FRAMEBUFFER_COMPLETE) {
////        NSLog(@"failed to make complete framebuffer object %x", status);
////        // :::: ALERT :::::
////        showAlert(@"FrameBuffer alert",[NSMutableString stringWithFormat:@"failed to make complete framebuffer object %x", status]);
////        
////    }
////    d->frameBuffer = framebuffer;
////    d->UVTexture = UVTexture;
////    d->colorTexture = colorTexture;
////    
////    glBindFramebuffer(GL_FRAMEBUFFER, 0);
////    //glDrawBuffers(1,DrawBuffers);
////
////    /*
////     // not sure where this comes from:
////     glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, framebuffer);
////     glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, 0);
////     glBlitFramebuffer(0, 0, fbWidth, fbHeight, 0, 0, fbWidth, fbHeight,
////                      GL_DEPTH_BUFFER_BIT, GL_NEAREST);
////    */
////    
////    
////    
////
////    
////    
////    ////////////////////////////
////    ////////////////////////////
////    ////////////////////////////
////    // TASK 2 ended
////    ////////////////////////////
////    ////////////////////////////
////    ////////////////////////////
////    
}






















void MeshRenderer::showAlert (NSString* title,NSString* text)
{
    if(true){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                         message:text
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
        [alert show];
    }
}



void MeshRenderer::enableVertexBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->vertexVbo[meshIndex]);
    glEnableVertexAttribArray(CustomShader::ATTRIB_VERTEX);
    glVertexAttribPointer(CustomShader::ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void MeshRenderer::disableVertexBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->vertexVbo[meshIndex]);
    glDisableVertexAttribArray(CustomShader::ATTRIB_VERTEX);
}

void MeshRenderer::enableNormalBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->normalsVbo[meshIndex]);
    glEnableVertexAttribArray(CustomShader::ATTRIB_NORMAL);
    glVertexAttribPointer(CustomShader::ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void MeshRenderer::disableNormalBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->normalsVbo[meshIndex]);
    glDisableVertexAttribArray(CustomShader::ATTRIB_NORMAL);
}

void MeshRenderer::enableVertexColorBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->colorsVbo[meshIndex]);
    glEnableVertexAttribArray(CustomShader::ATTRIB_COLOR);
    glVertexAttribPointer(CustomShader::ATTRIB_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void MeshRenderer::disableVertexColorBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->colorsVbo[meshIndex]);
    glDisableVertexAttribArray(CustomShader::ATTRIB_COLOR);
}

void MeshRenderer::enableVertexTexcoordsBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->texcoordsVbo[meshIndex]);
    glEnableVertexAttribArray(CustomShader::ATTRIB_TEXCOORD);
    glVertexAttribPointer(CustomShader::ATTRIB_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
}

void MeshRenderer::disableVertexTexcoordBuffer (int meshIndex)
{
    glBindBuffer(GL_ARRAY_BUFFER, d->texcoordsVbo[meshIndex]);
    glDisableVertexAttribArray(CustomShader::ATTRIB_TEXCOORD);
}

void MeshRenderer::enableLinesElementBuffer (int meshIndex)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d->linesVbo[meshIndex]);
    glLineWidth(1.0);
}

void MeshRenderer::enableTrianglesElementBuffer (int meshIndex)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d->facesVbo[meshIndex]);
}

void MeshRenderer::renderPartialMesh(int meshIndex)
{
    if (d->numTriangleIndices[meshIndex] <= 0) // nothing uploaded.
        return;
    
    switch (d->currentRenderingMode)
    {
        case RenderingModeXRay:
        {
            enableLinesElementBuffer(meshIndex);
            enableVertexBuffer(meshIndex);
            enableNormalBuffer(meshIndex);
            glDrawElements(GL_LINES, d->numLinesIndices[meshIndex], GL_UNSIGNED_SHORT, 0);
            disableNormalBuffer(meshIndex);
            disableVertexBuffer(meshIndex);
            break;
        }
            
        case RenderingModeLightedGray:
        {
            enableTrianglesElementBuffer(meshIndex);
            enableVertexBuffer(meshIndex);
            enableNormalBuffer(meshIndex);
            glDrawElements(GL_TRIANGLES, d->numTriangleIndices[meshIndex], GL_UNSIGNED_SHORT, 0);
            disableNormalBuffer(meshIndex);
            disableVertexBuffer(meshIndex);
            break;
        }
            
        case RenderingModePerVertexColor:
        {
            enableTrianglesElementBuffer(meshIndex);
            enableVertexBuffer(meshIndex);
            enableNormalBuffer(meshIndex);
            enableVertexColorBuffer(meshIndex);
            glDrawElements(GL_TRIANGLES, d->numTriangleIndices[meshIndex], GL_UNSIGNED_SHORT, 0);
            disableVertexColorBuffer(meshIndex);
            disableNormalBuffer(meshIndex);
            disableVertexBuffer(meshIndex);
            break;
        }

        case RenderingModeTextured:
        {
            enableTrianglesElementBuffer(meshIndex);
            enableVertexBuffer(meshIndex);
            enableVertexTexcoordsBuffer(meshIndex);
            glDrawElements(GL_TRIANGLES, d->numTriangleIndices[meshIndex], GL_UNSIGNED_SHORT, 0);
            disableVertexTexcoordBuffer(meshIndex);
            disableVertexBuffer(meshIndex);
            break;
        }

        default:
            NSLog(@"Unknown rendering mode.");
            break;
    }
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void MeshRenderer::render(const GLKMatrix4& projectionMatrix, const GLKMatrix4& modelViewMatrix)
{
    
    GLenum DrawBuffers[2]= {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT0+1};

    if (d->currentRenderingMode == RenderingModePerVertexColor && !d->hasPerVertexColor && d->hasTexture && d->hasPerVertexUV)
    {
        NSLog(@"Warning: The mesh has no per-vertex colors, but a texture, switching the rendering mode to RenderingModeTextured");
        d->currentRenderingMode = RenderingModeTextured;
    }
    else if (d->currentRenderingMode == RenderingModeTextured && (!d->hasTexture || !d->hasPerVertexUV) && d->hasPerVertexColor)
    {
        NSLog(@"Warning: The mesh has no texture, but per-vertex colors, switching the rendering mode to RenderingModePerVertexColor");
        d->currentRenderingMode = RenderingModePerVertexColor;
    }
    
    switch (d->currentRenderingMode)
    {
        case RenderingModeXRay:
            d->xRayShader.enable();
            d->xRayShader.prepareRendering(projectionMatrix.m, modelViewMatrix.m);
            break;
            
        case RenderingModeLightedGray:
            d->lightedGrayShader.enable();
            d->lightedGrayShader.prepareRendering(projectionMatrix.m, modelViewMatrix.m);
            break;

        case RenderingModePerVertexColor:
            if (!d->hasPerVertexColor)
            {
                NSLog(@"Warning: the mesh has no colors, skipping rendering.");
                return;
            }
            d->perVertexColorShader.enable();
            d->perVertexColorShader.prepareRendering(projectionMatrix.m, modelViewMatrix.m);
            break;
            
        case RenderingModeTextured:
            //if (!d->hasTexture || d->lumaTexture == NULL || d->chromaTexture == NULL || d->maskTexture == NULL) //----------------------------------
            
            
            if (!d->hasTexture || d->lumaTexture == NULL || d->chromaTexture == NULL)

            {
                NSLog(@"Warning: null textures, skipping rendering.");
                return;
            }
            
            glBindFramebuffer(GL_FRAMEBUFFER, d->frameBuffer);
            //glDrawBuffers(2,DrawBuffers);
            
            glActiveTexture(d->textureUnit);
            glBindTexture(CVOpenGLESTextureGetTarget(d->lumaTexture),
                          CVOpenGLESTextureGetName(d->lumaTexture));
            
            glActiveTexture(d->textureUnit + 1);
            glBindTexture(CVOpenGLESTextureGetTarget(d->chromaTexture),
                          CVOpenGLESTextureGetName(d->chromaTexture));
            
            glActiveTexture(d->textureUnit + 2);  //----------------------------------------------------------------------------
            glBindTexture(CVOpenGLESTextureGetTarget(d->maskTexture),
                          CVOpenGLESTextureGetName(d->maskTexture));
            
            
            d->yCbCrTextureShader.enable();
            d->yCbCrTextureShader.prepareRendering(projectionMatrix.m, modelViewMatrix.m, d->textureUnit);
            
            break;

        default:
            NSLog(@"Unknown rendering mode.");
            return;
    }
    
    // Keep previous GL_DEPTH_TEST state
    BOOL wasDepthTestEnabled = glIsEnabled(GL_DEPTH_TEST);
    glEnable(GL_DEPTH_TEST);
    
    for (int i = 0; i < d->numUploadedMeshes; ++i)
    {
        renderPartialMesh (i);
    }
    
    if(d->currentRenderingMode == RenderingModeTextured){

        /*
         
         // it doesnt work, check downloaded repository for esMatrixLoadIdentity-like functions
        
        
        // generate an orthographic matrix
        esMatrixLoadIdentity( &projectionMatrix );
        esOrtho(&projectionMatrix, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
        // generate a model view
        esMatrixLoadIdentity(&modelviewMatrix);
        // compute the final MVP
        esMatrixMultiply(&modelviewProjectionMatrix, &modelviewMatrix, &projectionMatrix);
        // setting up the viewport
        glViewport(0, 0, width, height);
        // binding
        glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
        glViewport(0, 0, backingWidth, backingHeight);
        
        glClearColor(0, 0, 0, 0);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        glEnable(GL_TEXTURE_2D);
        glActiveTexture(0);
        glBindTexture(GL_TEXTURE_2D, videoTexture);
        
        // setting up shader
        useProgram(projectionProgram);
        
        // updating uniform values
        GLint uMVPIndex      = indexUniform(projectionProgram, "mvpMatrix");
        GLint uTextureIndex  = indexUniform(projectionProgram, "textureImg");
        glUniformMatrix4fv( uMVPIndex, 1, GL_FALSE, (GLfloat*) &modelviewProjectionMatrix.m[0][0] );
        glUniform1i(uTextureIndex, 0);
        
        // updating attribute values
        GLint aPositionIndex = indexAttribute(projectionProgram, "position");
        GLint aTextureIndex  = indexAttribute(projectionProgram, "inputTextureCoordinate");
        
        // drawing quad
        static int strideVertex  = 2*sizeof(GLfloat); // 2D position
        static int strideTexture = 2*sizeof(GLfloat); // 2D texture coordinates
        
        // beginning of arrays
        const GLvoid* vertices  = (const GLvoid*) &screenVertices[0];
        const GLvoid* textures  = (const GLvoid*) &texCoordsFullScreen [0];
        
        // enabling vertex arrays
        glVertexAttribPointer(aPositionIndex, 2, GL_FLOAT, GL_FALSE, strideVertex, vertices);
        glEnableVertexAttribArray(aPositionIndex);
        glVertexAttribPointer(aTextureIndex, 2, GL_FLOAT, GL_FALSE, strideTexture, textures);
        glEnableVertexAttribArray(aTextureIndex);
        
        // drawing
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
        // resetting matrices
        esMatrixLoadIdentity(&projectionMatrix);
        esMatrixLoadIdentity(&modelviewMatrix);
        esMatrixLoadIdentity(&modelviewProjectionMatrix);
        // resetting shader
        releaseProgram(projectionProgram);
        // unbinding texture
        glBindTexture(GL_TEXTURE_2D, 0);
        
        */
        
        
        
        
    }

    
    if (!wasDepthTestEnabled)
        glDisable(GL_DEPTH_TEST);
    
    
    
    
    
}



